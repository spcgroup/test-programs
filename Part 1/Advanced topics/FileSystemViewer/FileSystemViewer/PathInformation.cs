﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Linq;

namespace FileSystemViewer
{
    public class PathInformation
    {
        public string FullPath { get; private set; }
        public string Name { get; private set; }
        public DateTime Created { get; private set; }
        public long Size { get; set; }
        public List<PathInformation> SubFolders { get; private set; } = new List<PathInformation>();
        public List<PathInformation> SubFiles { get; private set; } = new List<PathInformation>();
                
        public PathInformation(string path)
        {
            if(File.Exists(path)||Directory.Exists(path))
            {
                FullPath = Path.GetFullPath(path);
                Name = Path.GetFileName(path) ?? path;
                Created = File.GetCreationTime(path);
            }
        }
       
        public XElement ToXElement()
        {
            return new XElement(PathInformationHelper.PATH_INFORMATION_XML_STRING,
                        new XAttribute(PathInformationHelper.FULL_PATH_XML_STRING, FullPath),
                        new XAttribute(PathInformationHelper.NAME_XML_STRING, Name),
                        new XAttribute(PathInformationHelper.CREATED_XML_STRING, Created.ToString()),
                        new XAttribute(PathInformationHelper.SIZE_XML_STRING, Size)
                        );
        }
    }
}
