﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemViewer
{
    public class ExceptionEventArgs : EventArgs
    {
        public string Message { get; set; }

        public ExceptionEventArgs(string message)
        {
            Message = message;
        }
    }
}
