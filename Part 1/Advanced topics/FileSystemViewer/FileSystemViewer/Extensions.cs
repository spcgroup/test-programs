﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace FileSystemViewer
{
    public static class Extensions
    {
        public static void HandleExeption(this ISynchronizeInvoke instance,object sender, ExceptionEventArgs e)
        {
            string outText = $"An exception has occured in thread: {Thread.CurrentThread.Name}" +
                Environment.NewLine + $"Details: {e.Message}" + Environment.NewLine+ Environment.NewLine;
            (instance as TextBox).Invoke((Action)delegate
            {
                (instance as TextBox).Text += outText;                
            });
                  
        }
    }
}
