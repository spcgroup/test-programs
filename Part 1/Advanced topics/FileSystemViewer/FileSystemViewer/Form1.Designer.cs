﻿namespace FileSystemViewer
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderTextBox = new System.Windows.Forms.TextBox();
            this.filesTreeView = new System.Windows.Forms.TreeView();
            this.outFileTextBox = new System.Windows.Forms.TextBox();
            this.outFileSelectButton = new System.Windows.Forms.Button();
            this.folderSelectButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.outputTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pathInfoLabel = new System.Windows.Forms.Label();
            this.startScanButton = new System.Windows.Forms.Button();
            this.scanlogLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // folderTextBox
            // 
            this.folderTextBox.Location = new System.Drawing.Point(13, 23);
            this.folderTextBox.Name = "folderTextBox";
            this.folderTextBox.Size = new System.Drawing.Size(291, 20);
            this.folderTextBox.TabIndex = 5;
            this.folderTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnTextBoxEnterKeyPressed);
            // 
            // filesTreeView
            // 
            this.filesTreeView.Location = new System.Drawing.Point(11, 144);
            this.filesTreeView.Name = "filesTreeView";
            this.filesTreeView.Size = new System.Drawing.Size(390, 387);
            this.filesTreeView.TabIndex = 2;
            this.filesTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.filesTreeView_AfterSelect);
            // 
            // outFileTextBox
            // 
            this.outFileTextBox.Location = new System.Drawing.Point(13, 64);
            this.outFileTextBox.Name = "outFileTextBox";
            this.outFileTextBox.Size = new System.Drawing.Size(291, 20);
            this.outFileTextBox.TabIndex = 3;
            this.outFileTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnTextBoxEnterKeyPressed);
            // 
            // outFileSelectButton
            // 
            this.outFileSelectButton.Location = new System.Drawing.Point(310, 58);
            this.outFileSelectButton.Name = "outFileSelectButton";
            this.outFileSelectButton.Size = new System.Drawing.Size(93, 31);
            this.outFileSelectButton.TabIndex = 4;
            this.outFileSelectButton.Text = "Выбрать файл";
            this.outFileSelectButton.UseVisualStyleBackColor = true;
            this.outFileSelectButton.Click += new System.EventHandler(this.outFileSelectButton_Click);
            // 
            // folderSelectButton
            // 
            this.folderSelectButton.Location = new System.Drawing.Point(310, 17);
            this.folderSelectButton.Name = "folderSelectButton";
            this.folderSelectButton.Size = new System.Drawing.Size(93, 31);
            this.folderSelectButton.TabIndex = 1;
            this.folderSelectButton.Text = "Выбрать папку";
            this.folderSelectButton.UseVisualStyleBackColor = true;
            this.folderSelectButton.Click += new System.EventHandler(this.folderSelectButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "XML файл результатов:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(205, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Папка, в которой производится поиск:";
            // 
            // outputTextBox
            // 
            this.outputTextBox.Location = new System.Drawing.Point(410, 144);
            this.outputTextBox.Multiline = true;
            this.outputTextBox.Name = "outputTextBox";
            this.outputTextBox.ReadOnly = true;
            this.outputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.outputTextBox.Size = new System.Drawing.Size(320, 387);
            this.outputTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(409, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Ошибки:";
            // 
            // pathInfoLabel
            // 
            this.pathInfoLabel.Location = new System.Drawing.Point(409, 7);
            this.pathInfoLabel.Name = "pathInfoLabel";
            this.pathInfoLabel.Size = new System.Drawing.Size(321, 121);
            this.pathInfoLabel.TabIndex = 9;
            // 
            // startScanButton
            // 
            this.startScanButton.Location = new System.Drawing.Point(310, 96);
            this.startScanButton.Name = "startScanButton";
            this.startScanButton.Size = new System.Drawing.Size(93, 31);
            this.startScanButton.TabIndex = 10;
            this.startScanButton.Text = "Сканировать";
            this.startScanButton.UseVisualStyleBackColor = true;
            this.startScanButton.Click += new System.EventHandler(this.startScanButton_Click);
            // 
            // scanInfoLabel
            // 
            this.scanlogLabel.AutoSize = true;
            this.scanlogLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.scanlogLabel.ForeColor = System.Drawing.Color.Green;
            this.scanlogLabel.Location = new System.Drawing.Point(27, 87);
            this.scanlogLabel.Name = "scanInfoLabel";
            this.scanlogLabel.Size = new System.Drawing.Size(0, 13);
            this.scanlogLabel.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 128);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Дерево файлов:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 538);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.scanlogLabel);
            this.Controls.Add(this.startScanButton);
            this.Controls.Add(this.pathInfoLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.outputTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.folderSelectButton);
            this.Controls.Add(this.outFileSelectButton);
            this.Controls.Add(this.outFileTextBox);
            this.Controls.Add(this.filesTreeView);
            this.Controls.Add(this.folderTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox folderTextBox;
        private System.Windows.Forms.TreeView filesTreeView;
        private System.Windows.Forms.TextBox outFileTextBox;
        private System.Windows.Forms.Button outFileSelectButton;
        private System.Windows.Forms.Button folderSelectButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox outputTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label pathInfoLabel;
        private System.Windows.Forms.Button startScanButton;
        private System.Windows.Forms.Label scanlogLabel;
        private System.Windows.Forms.Label label4;
    }
}

