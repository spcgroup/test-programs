﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.IO;

namespace FileSystemViewer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitializeBackgroundThreads();
        }

        ThreadManager threadManager = new ThreadManager();

        object _scanLogLabelLock = new object();       

        private void InitializeBackgroundThreads()
        {
            threadManager.OnExceptionGenerated += outputTextBox.HandleExeption;
            threadManager.StartBackgroundThread(new ThreadStart(FillUpTreeView),"VisualThread");
            threadManager.StartBackgroundThread(new ThreadStart(threadManager.WriteToFile),"WriterThread");

            threadManager.OnScanningStarted += (s, e) =>
            {
                Invoke((Action)delegate
                {
                    lock (_scanLogLabelLock)
                    {
                        scanlogLabel.Text = "Идет сканирование...";
                    }
                    outputTextBox.Text = string.Empty;
                });
            };

            threadManager.OnScanningComplete += (s, e) =>
            {
                Invoke((Action)delegate
                {
                    lock (_scanLogLabelLock)
                    {
                        scanlogLabel.Text = "Сканирование - завершено" + Environment.NewLine;
                    }
                });
            };

            string writeStartText = string.Empty;

            threadManager.OnWritingStarted += (s, e) =>
            {
                Invoke((Action)delegate
                {
                    writeStartText = s.ToString();
                    lock (_scanLogLabelLock)
                    {
                        scanlogLabel.Text += s.ToString();
                    }
                });
            };

            threadManager.OnWritingComplete += (s, e) =>
            {
                Invoke((Action)delegate
                {
                    lock (_scanLogLabelLock)
                    {
                        scanlogLabel.Text = scanlogLabel.Text.Replace(writeStartText, s.ToString());
                    }
                });
            };

            threadManager.OnBackgroundWorkComplete += (s, e) =>
            {
                Invoke((Action)delegate
                {
                    startScanButton.Enabled = true;
                });
            };
        }
       
        private void FillUpTreeView()
        {
            string startedLogText = "Идет построение дерева файлов..." + Environment.NewLine;
            string endedLogText = "Построение дерева файлов - завершено" + Environment.NewLine;
            string errorLogText = "Построение дерева файлов - ошибка" + Environment.NewLine;

            while (true)
            {
                try
                {
                    threadManager._itemAdded.WaitOne();
                    threadManager._drawingComplete.Set();
                    TreeNode rootNode = new TreeNode();
                    Invoke((Action)delegate
                    {
                        lock(_scanLogLabelLock)
                        {
                            scanlogLabel.Text += startedLogText;
                        }
                    });
                    string first = threadManager.InitialDirectory;
                    string fileName = threadManager.PathToXmlFile;
                    
                    lock (threadManager._collectionLock)
                    {
                        rootNode.Name = threadManager.PathInfoRoot.FullPath;
                        rootNode.Text = threadManager.PathInfoRoot.Name;
                        rootNode.Tag = threadManager.PathInfoRoot;
                        AddNodes(rootNode, threadManager.PathInfoRoot);
                    }
                    Invoke((Action)delegate
                    {
                        filesTreeView.Nodes.Clear();
                        filesTreeView.Nodes.Add(rootNode);
                        lock (_scanLogLabelLock)
                        {
                            scanlogLabel.Text = scanlogLabel.Text.Replace(startedLogText, endedLogText);
                        }
                    });
                }
                catch (Exception ex)
                {
                    Invoke((Action)delegate
                    {
                    lock (_scanLogLabelLock)
                        {
                            scanlogLabel.Text = scanlogLabel.Text.Replace(startedLogText, errorLogText);
                        }
                    });
                    throw ex;
                }
                finally
                {    
                    threadManager._drawingComplete.Set();
                }
            }
        }

        private void AddNodes(TreeNode root, PathInformation rootInfo)
        {
            foreach (PathInformation info in rootInfo.SubFolders)
            {
                TreeNode tempNode = new TreeNode()
                {
                    Name = info.FullPath,
                    Text = info.Name,
                    Tag = info
                };
                root.Nodes.Add(tempNode);
                AddNodes(tempNode, info);
            }
            foreach (PathInformation info in rootInfo.SubFiles)
            {
                root.Nodes.Add(new TreeNode()
                {
                    Name = info.FullPath,
                    Text = info.Name,
                    Tag = info
                });
            }
        }

        private void folderSelectButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderSelector = new FolderBrowserDialog();
            if(folderSelector.ShowDialog()==DialogResult.OK)
            {
                folderTextBox.Text = folderSelector.SelectedPath;                                
            }
        }

        private void OnTextBoxEnterKeyPressed(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
            {
                e.SuppressKeyPress = true;
                SelectNextControl(ActiveControl, true, true, true, true);
            }
        }

        private void outFileSelectButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "XML files | *.xml";
            if(openDialog.ShowDialog()==DialogResult.OK)
            {
                outFileTextBox.Text = openDialog.FileName;                
            }
        }

        private void filesTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            PathInformation curInfo = e.Node.Tag as PathInformation;
            pathInfoLabel.Text = $"Имя: {curInfo.Name}" + Environment.NewLine +
                $"Полный путь: {curInfo.FullPath}" + Environment.NewLine +
                $"Дата создания: {curInfo.Created.ToString()}" + Environment.NewLine+
                $"Размер: {curInfo.Size / 1024} КБ";            
        }

        private void startScanButton_Click(object sender, EventArgs e)
        {
            if(Directory.Exists(folderTextBox.Text) && outFileTextBox.Text.Trim().EndsWith(".xml")
                && outFileTextBox.Text.IndexOfAny(Path.GetInvalidPathChars()) == -1)
            {
                try
                {
                    threadManager.InitialDirectory = folderTextBox.Text;
                    threadManager.PathToXmlFile = outFileTextBox.Text;
                    threadManager.StartScanningThread(folderTextBox.Text);
                    startScanButton.Enabled = false;
                }                
                catch(OperationCanceledException)
                {
                    MessageBox.Show("Сканирование уже запущено", "Ошибка");
                    startScanButton.Enabled = true;
                }
            }
            else
            {
                MessageBox.Show("Имя папки или файла результатов заданы неверно." + Environment.NewLine +
                    "Убедитесь что выбранная папка существует, а имя файла результатов" + Environment.NewLine +
                    "не содержит недопустимых символов и имеет расширение .XML", "Ошибка");
            }
        }
    }
}
