﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemViewer
{
    public static class PathInformationHelper
    {
        public const string PATH_INFORMATION_XML_STRING = "Entry";
        public const string FULL_PATH_XML_STRING = "fullPath";
        public const string NAME_XML_STRING = "fileName";
        public const string SIZE_XML_STRING = "size";
        public const string CREATED_XML_STRING = "created";
    }
}
