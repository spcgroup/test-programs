﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml.Linq;
using System.ComponentModel;

namespace FileSystemViewer
{
    class ThreadManager
    {
        
        public object _collectionLock = new object(); 
        
        public PathInformation PathInfoRoot { get; private set; }
        
        public string PathToXmlFile { get; set; }
        
        public string InitialDirectory { get; set; }


        private XElement _xmlRoot;

        private Thread _scanThread;


        public ManualResetEvent _itemAdded = new ManualResetEvent(false);

        private AutoResetEvent _writingComplete = new AutoResetEvent(false);

        public AutoResetEvent _drawingComplete = new AutoResetEvent(false);
        
        public event EventHandler<ExceptionEventArgs> OnExceptionGenerated;
        
        public event EventHandler OnScanningStarted;

        public event EventHandler OnScanningComplete;

        public event EventHandler OnWritingStarted;

        public event EventHandler OnWritingComplete;

        public event EventHandler OnBackgroundWorkComplete;


        public void StartScanningThread(string path)
        {
            if (_scanThread == null || !_scanThread.IsAlive)
            {                
                _scanThread = new Thread(() =>
                {  
                    OnScanningStarted(this, EventArgs.Empty);                   
                    lock (_collectionLock)
                    {
                        PathInfoRoot = new PathInformation(path);
                        ScanDirectory(PathInfoRoot);
                    }
                    OnScanningComplete(this, EventArgs.Empty);
                    _itemAdded.Set();                  
                    WaitHandle.WaitAll(new WaitHandle[] { _writingComplete, _drawingComplete });
                    _itemAdded.Reset();
                    WaitHandle.WaitAll(new WaitHandle[] { _writingComplete, _drawingComplete });
                    OnBackgroundWorkComplete(this, EventArgs.Empty);
                });
                _scanThread.IsBackground = true;
                _scanThread.Name = "ScanThread";
                _scanThread.Start();
            } 
            else
            {
                throw new OperationCanceledException();
            }           
        }
                
        private void ScanDirectory(PathInformation folderInfo)
        {
            try
            {
                foreach (string dir in Directory.GetDirectories(folderInfo.FullPath))
                {
                    PathInformation tempDirInfo = new PathInformation(dir);
                    folderInfo.SubFolders.Add(tempDirInfo);
                    ScanDirectory(tempDirInfo);
                    folderInfo.Size += tempDirInfo.Size;
                }
                foreach (string file in Directory.GetFiles(folderInfo.FullPath))
                {
                    try
                    {
                        FileInfo curFileInfo = new FileInfo(file);
                        PathInformation tempFile = new PathInformation(file);
                        folderInfo.SubFiles.Add(tempFile);
                        tempFile.Size = curFileInfo.Length;
                        folderInfo.Size += curFileInfo.Length;
                    }
                    catch (Exception e)
                    {
                        OnExceptionGenerated(this, new ExceptionEventArgs(e.Message));
                    }
                }
            }
            catch (Exception e)
            {
                OnExceptionGenerated(this, new ExceptionEventArgs(e.Message));
            }
        }              

        public void StartBackgroundThread(ThreadStart workMethod, string name)
        {
            Thread t = new Thread(()=>
            {
                try
                {
                    workMethod();
                }
                catch(Exception ex)
                {
                    OnExceptionGenerated(this, new ExceptionEventArgs(ex.Message));
                }
            });
            t.IsBackground = true;
            t.Name = name;
            t.Start();
        }

        public void WriteToFile()
        {
            string startWriteLogText = "Идет запись в файл..." + Environment.NewLine;
            string endWriteLogText = "Запись в файл - завершено" + Environment.NewLine;
            string errorLogText = "Запись в файл - ошибка" + Environment.NewLine; 
                    
            while (true)
            {
                try
                {
                    _itemAdded.WaitOne();
                    _writingComplete.Set();                    
                    OnWritingStarted(startWriteLogText, EventArgs.Empty);
                    string first = InitialDirectory;
                    string fileName = PathToXmlFile;
                    lock (_collectionLock)
                    {
                        _xmlRoot = PathInfoRoot.ToXElement();
                        AddElements(_xmlRoot, PathInfoRoot);
                    }
                    if (fileName != string.Empty)
                    {
                        _xmlRoot.Save(fileName);
                        OnWritingComplete(endWriteLogText, EventArgs.Empty);
                    }
                }
                catch(Exception ex)
                {
                    OnWritingComplete(errorLogText, EventArgs.Empty);
                    throw ex;
                }
                finally
                {
                    _writingComplete.Set();
                }
            }
        }

        private void AddElements(XElement root, PathInformation rootInfo)
        {
            foreach (PathInformation info in rootInfo.SubFolders)
            {
                XElement tempElement = info.ToXElement();
                root.Add(tempElement);
                AddElements(tempElement, info);
            }
            foreach (PathInformation info in rootInfo.SubFiles)
            {
                root.Add(info.ToXElement());
            }
        }
    }
}
