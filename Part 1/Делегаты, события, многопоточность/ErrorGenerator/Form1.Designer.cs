﻿namespace ErrorGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.logTextBox = new System.Windows.Forms.TextBox();
            this.backThread1Activator = new System.Windows.Forms.CheckBox();
            this.backThread2Activator = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // logTextBox
            // 
            this.logTextBox.Location = new System.Drawing.Point(12, 44);
            this.logTextBox.Multiline = true;
            this.logTextBox.Name = "logTextBox";
            this.logTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.logTextBox.Size = new System.Drawing.Size(568, 452);
            this.logTextBox.TabIndex = 0;
            // 
            // subThread1Activator
            // 
            this.backThread1Activator.AutoSize = true;
            this.backThread1Activator.Location = new System.Drawing.Point(27, 14);
            this.backThread1Activator.Name = "subThread1Activator";
            this.backThread1Activator.Size = new System.Drawing.Size(204, 17);
            this.backThread1Activator.TabIndex = 1;
            this.backThread1Activator.Text = "Генерировать ошибки FileNotFound";
            this.backThread1Activator.UseVisualStyleBackColor = true;
            this.backThread1Activator.CheckedChanged += new System.EventHandler(this.subThread1Activator_CheckedChanged);
            // 
            // subThread2Activator
            // 
            this.backThread2Activator.AutoSize = true;
            this.backThread2Activator.Location = new System.Drawing.Point(306, 14);
            this.backThread2Activator.Name = "subThread2Activator";
            this.backThread2Activator.Size = new System.Drawing.Size(227, 17);
            this.backThread2Activator.TabIndex = 2;
            this.backThread2Activator.Text = "Генерировать ошибки IndexOutOfRange";
            this.backThread2Activator.UseVisualStyleBackColor = true;
            this.backThread2Activator.CheckedChanged += new System.EventHandler(this.subThread2Activator_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 508);
            this.Controls.Add(this.backThread2Activator);
            this.Controls.Add(this.backThread1Activator);
            this.Controls.Add(this.logTextBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox logTextBox;
        private System.Windows.Forms.CheckBox backThread1Activator;
        private System.Windows.Forms.CheckBox backThread2Activator;
    }
}

