﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ErrorGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            Program.ExceptionGenerated += OnExceptionGenerated;
            InitializeComponent();
        }

        private void OnExceptionGenerated(object sender, EventArgs e)
        {   
            //передача управления в основной поток        
            Invoke((Action)delegate
            {   
                lock (Program.errorQueueLock)
                {
                    logTextBox.Text += Program.ErrorQueue.Dequeue() + Environment.NewLine;
                }
            });
        }

        private void Form1_Load(object sender, EventArgs e)
        {            
            logTextBox.Text += "Main thread started" + Environment.NewLine;
        }

        private void subThread1Activator_CheckedChanged(object sender, EventArgs e)
        {
            //не уверен что тут нужна блокировка, т.к. на работу это не влияет
            lock (Program.isBackThread1WorkingLock)
            {
                Program.IsBackThread1Working = backThread1Activator.Checked;
            }
        }

        private void subThread2Activator_CheckedChanged(object sender, EventArgs e)
        {
            //не уверен что тут нужна блокировка, т.к. на работу это не влияет
            lock (Program.isBackThread2WorkingLock)
            {
                Program.IsBackThread2Working = backThread2Activator.Checked;
            }
        }
    }
}
