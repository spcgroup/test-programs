﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice
{
    class Program
    {
        static void Main(string[] args)
        {
            PrintUsingForeach();
            PrintUsingFor();
            PrintUsingWhile();
            PrintUsingDoWhile();
            Console.ReadKey();
        }

        static void PrintUsingForeach()
        {
            Console.WriteLine("foreach:");
            int[] numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            foreach (int i in numbers)
            {
                foreach (int j in numbers)
                {
                    PrintNumber(i, j);
                }
                Console.Write("\n");
            }
        }

        static void PrintUsingFor()
        {
            Console.WriteLine("for:");
            for (int i = 1; i <= 10; i++)
            {
                for (int j = 1; j <= 10; j++)
                {
                    PrintNumber(i, j);
                }
                Console.Write("\n");
            }
        }

        static void PrintUsingWhile()
        {
            Console.WriteLine("while:");
            int i = 0;
            while (++i <= 10)
            {
                int j = 0;
                while (++j <= 10)
                {
                    PrintNumber(i, j);
                }
                Console.Write("\n");
            }
        }

        static void PrintUsingDoWhile()
        {
            Console.WriteLine("do-while:");
            int i = 1;
            do
            {
                int j = 1;
                do
                {
                    PrintNumber(i, j);
                } while (++j <= 10);
                Console.Write("\n");
            } while (++i <= 10);
        }

        static void PrintNumber(int i, int j)
        {
            if (i == j)
            {
                ConsoleColor curBack = Console.BackgroundColor,
                    curFore = Console.ForegroundColor;
                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.Write("{0,3}", i * i);
                Console.BackgroundColor = curBack;
                Console.ForegroundColor = curFore;
                Console.Write("\t");
            }
            else
            {
                Console.Write("{0,3}\t", i * j);
            }
        }
    }
}
