﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace practice
{
    class Program
    {
        static ArrayList int_array = new ArrayList();
        static ArrayList string_array = new ArrayList();
        static List<int> int_list = new List<int>();
        static List<string> string_list = new List<string>();

        static void Main(string[] args)
        {
            Console.WriteLine("testing collections\n");           
            Console.WriteLine("adding elements \n");
            TestFill(100000);
            Console.WriteLine("\ngetting elements \n");
            TestGet(100000);
            Console.WriteLine("\nsorlting lists\n");
            TestSort();
            Console.Read();
        }
        
        static void TestFill(int count)
        {
            Stopwatch timer;
            double average;

            timer = Stopwatch.StartNew();//start measuring
            for (int i = 0; i < count; i++)              
                int_array.Add(i);
            timer.Stop();//stop measuring
            average = timer.ElapsedTicks / (double)count;
            Console.WriteLine("Add {0} int elements to ArrayList (average ticks): {1}",
                count, average);

            timer = Stopwatch.StartNew();            
            for (int i = 0; i < count; i++)
                int_list.Add(i);
            timer.Stop();
            average = timer.ElapsedTicks /(double) count;
            Console.WriteLine("Add {0} int elements to List<int> (average ticks): {1}",
                count, average);

            timer = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
                string_array.Add(i.ToString());
            timer.Stop();
            average = timer.ElapsedTicks /(double) count;
            Console.WriteLine("Add {0} string elements to ArrayList (average ticks): {1}",
                count, average);

            timer = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
                string_list.Add(i.ToString());
            timer.Stop();
            average = timer.ElapsedTicks /(double) count;
            Console.WriteLine("Add {0} string elements to List<string> (average ticks): {1}",
                count, average);             
        }
        static void TestGet(int count)
        {
            Stopwatch timer;
            double average;
            int temp_int;
            string temp_string;

            timer = Stopwatch.StartNew();//start measuring 
            for (int i = 0; i < count; i++)
                temp_int=(int)int_array[i];
            timer.Stop();//stop measuring
            average = timer.ElapsedTicks / (double)count;
            Console.WriteLine("Get {0} int elements from ArrayList (average ticks): {1}",
                count, average);

            timer = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
                temp_int=int_list[i];
            timer.Stop();
            average = timer.ElapsedTicks / (double)count;
            Console.WriteLine("Get {0} int elements from List<int> (average ticks): {1}",
                count, average);

            timer = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
               temp_string = (string)string_array[i];
            timer.Stop();
            average = timer.ElapsedTicks / (double)count;
            Console.WriteLine("Get {0} string elements from ArrayList (average ticks): {1}",
                count, average);

            timer = Stopwatch.StartNew();
            for (int i = 0; i < count; i++)
                temp_string=string_list[i];
            timer.Stop();
            average = timer.ElapsedTicks / (double)count;
            Console.WriteLine("Get {0} string elements from List<string> (average ticks): {1}",
                count, average);
        }
        static void TestSort()
        {
            Stopwatch timer;           

            int_array.Reverse();
            timer = Stopwatch.StartNew();
            int_array.Sort();
            timer.Stop();
            Console.WriteLine("Sorting ArrayList of ints takes {0} ticks",timer.ElapsedTicks);

            int_list.Reverse();
            timer = Stopwatch.StartNew();
            int_list.Sort();
            timer.Stop();
            Console.WriteLine("Sorting List<int> takes {0} ticks", timer.ElapsedTicks);

            string_array.Reverse();
            timer = Stopwatch.StartNew();
            string_array.Sort();
            timer.Stop();
            Console.WriteLine("Sorting ArrayList of strings takes {0} ticks", timer.ElapsedTicks);

            string_list.Reverse();
            timer = Stopwatch.StartNew();
            string_list.Sort();
            timer.Stop();
            Console.WriteLine("Sorting List<string> takes {0} ticks", timer.ElapsedTicks);

        }
    }
}
