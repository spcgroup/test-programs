﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("testing wrapper on ints");
            SortedSet<GenericWrapper<int>> int_set  = new SortedSet<GenericWrapper<int>>
            {
                new GenericWrapper<int>(10),
                new GenericWrapper<int>(2),
                new GenericWrapper<int>(5)
            };
            foreach(GenericWrapper<int> i in int_set)
            {
                Console.WriteLine(i.Value.ToString());
            }

            Console.WriteLine("testing wrapper on TestStructs");
            SortedSet<GenericWrapper<TestStruct>> test_set = new SortedSet<GenericWrapper<TestStruct>>
            {
                new GenericWrapper<TestStruct>(new TestStruct { A=10, B=1 }),
                new GenericWrapper<TestStruct>(new TestStruct { A=2, B=2 }),
                new GenericWrapper<TestStruct>(new TestStruct { A=5, B=3 })
            };
            foreach (GenericWrapper<TestStruct> i in test_set)
            {
                Console.WriteLine(i.Value.ToString());
            }
            Console.ReadKey();
        }
    }
}
