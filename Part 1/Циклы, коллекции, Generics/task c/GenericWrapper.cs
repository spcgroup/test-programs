﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practice
{
    class GenericWrapper<T> : IComparable<GenericWrapper<T>>
        where T : struct, IComparable<T>
    {
        public T Value { get; set; }
        public GenericWrapper(T item)
        {
            Value = item;
        }
        
        public int CompareTo(GenericWrapper<T> other)
        {                  
            return  Value.CompareTo(other.Value);
        }
    }

    struct TestStruct : IComparable<TestStruct>
    {
        public int A { get; set; }
        public double B { get; set; }

        public int CompareTo(TestStruct other)
        {
            return A == other.A ? 0 : A > other.A ? 1 : - 1;// сортировка по полю А
        }
        public override string ToString()
        {
            return string.Format("{0}\t{1}",A,B);
        }
    }
}
