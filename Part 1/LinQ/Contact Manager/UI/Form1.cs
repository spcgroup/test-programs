﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.IO;

namespace ContactManager
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            contactManager.ContactsListChanged += Contacts_ContactsListChanged;                        
        }

        public ContactManager contactManager { get; set; } = new ContactManager();

        private void Contacts_ContactsListChanged(object collection)
        {            
            FillUpTree(collection);
        }

        //заполняет treeView, в зависимости от типа коллекции
        private void FillUpTree(object collection)
        {            
            contactsTree.BeginUpdate();
            contactsTree.Nodes.Clear();
            if (collection is IEnumerable<IGrouping<string, XElement>>)
            {
                var contacts = collection as IEnumerable<IGrouping<string, XElement>>;
                foreach (var group in contacts)
                {
                    if (group.Key != "")
                    {
                        contactsTree.Nodes.Add(group.Key);
                    }
                    else
                    {
                        contactsTree.Nodes.Add("Без группы");
                    }
                    foreach (XElement item in group)
                    {
                        contactsTree.Nodes[contactsTree.Nodes.Count - 1].Nodes.Add(
                            new TreeNode($"{item.Element(Contact._firstNameXmlString).Value} "+
                            $"{item.Element(Contact._lastNameXmlString).Value}")
                            {
                                //для каждого узла хранить ссылку на контакт
                                Tag = item
                            });
                    }
                }
            }
            if(collection is IEnumerable<XElement>)
            {
                var contacts = collection as IEnumerable<XElement>;
                foreach (XElement item in contacts)
                {
                    contactsTree.Nodes.Add(
                        new TreeNode($"{item.Element(Contact._firstNameXmlString).Value} "+
                        $"{item.Element(Contact._lastNameXmlString).Value}")
                        {
                            //для каждого узла хранить ссылку на контакт
                            Tag = item
                        });
                }
            }
            contactsTree.EndUpdate();
        }

        //создает новый контакт, используя данные из полей формы
        private Contact RetrieveContact()
        {
            exceptionTextLabel.Text = string.Empty;
            Contact c = new Contact()
            {
                FirstName = firstNameTextBox.Text,
                LastName = lastNameTextBox.Text,
                Group = groupTextBox.Text,
                HomePhoneNumber = homePhoneTextBox.Text,
                MobilePhoneNumber = mobPhoneTextBox.Text,
                PathToPhoto = pathToPhotoTextBox.Text
            };            
            try
            {
                ContactManager.ValidateContact(c);
                return c;
            }
            catch(ArgumentException ex)
            {                
                exceptionTextLabel.Text = ex.Message;               
                return null;
            }
            
        }

        //очищает поля ввода и фото контакта
        private void CleanForm()
        {
            firstNameTextBox.Text = "";
            lastNameTextBox.Text = "";
            groupTextBox.Text = "";
            homePhoneTextBox.Text = "";
            mobPhoneTextBox.Text = "";
            pathToPhotoTextBox.Text = "";
            photoBox.BackgroundImage = null;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            contactManager.Load();          
        }

        private void isGroupedCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            exceptionTextLabel.Text = string.Empty;
            contactManager.IsGrouped = isGroupedCheckBox.Checked;
            CleanForm();
            if (searchForm.Text.Trim() != string.Empty)//необходимо группировать результаты фильтрации
            {
                contactManager.Filter(searchForm.Text);
            }
            else
            {
                if (isGroupedCheckBox.Checked)
                {
                    FillUpTree(contactManager.GetGroupedContactsList());
                }
                else
                {
                    FillUpTree(contactManager.ContactsList.Elements());
                }
            }
            removeButton.Enabled = false;
            saveButton.Enabled = false;
        }

        //фильтрует список контактов по имени и фамилии
        private void searchForm_TextChanged(object sender, EventArgs e)
        {
            exceptionTextLabel.Text = string.Empty;
            contactManager.Filter(searchForm.Text);
            CleanForm();           
        }

        //заполняет поля формы при выборе контакта
        private void contactsTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            exceptionTextLabel.Text = string.Empty;
            if (e.Node.Tag is XElement)
            {
                XElement currElement = e.Node.Tag as XElement;
                firstNameTextBox.Text = currElement.Element(Contact._firstNameXmlString).Value;
                lastNameTextBox.Text = currElement.Element(Contact._lastNameXmlString).Value;
                groupTextBox.Text = currElement.Element(Contact._groupXmlString).Value;
                homePhoneTextBox.Text = currElement.Element(Contact._homePhoneXmlString).Value;
                mobPhoneTextBox.Text = currElement.Element(Contact._mobilePhoneXmlString).Value;
                try
                {
                    photoBox.BackgroundImage = new Bitmap(currElement.Element(Contact._pathToPhotoXmlString).Value);
                    pathToPhotoTextBox.Text = currElement.Element(Contact._pathToPhotoXmlString).Value;
                }
                catch
                {
                    photoBox.BackgroundImage = UI.Properties.Resources.not_found;
                    pathToPhotoTextBox.Text = string.Empty;
                }                
                removeButton.Enabled = true;
                saveButton.Enabled = true;                
            }
            else
            {
                CleanForm();
                addButton.Enabled = false;
                removeButton.Enabled = false;
                saveButton.Enabled = false;
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            exceptionTextLabel.Text = string.Empty;
            successLabel.Text = string.Empty;        
            contactManager.Add(RetrieveContact()?.ToXElement());            
            if (exceptionTextLabel.Text == string.Empty)// ошибок не было
            {
                CleanForm();                                
                successLabel.Text = "Контакт добавлен";                
            }
            addButton.Enabled = false;
            removeButton.Enabled = false;
            saveButton.Enabled = false;
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            exceptionTextLabel.Text = string.Empty;
            successLabel.Text = string.Empty;
            if (contactsTree.SelectedNode?.Tag is XElement)
            {
                contactManager.Remove(contactsTree.SelectedNode.Tag as XElement);
                CleanForm();
                addButton.Enabled = false;
                removeButton.Enabled = false;
                saveButton.Enabled = false;
                successLabel.Text = "Контакт удален";
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            exceptionTextLabel.Text = string.Empty;
            successLabel.Text = string.Empty;
            if (contactsTree.SelectedNode?.Tag is XElement)
            {                
                contactManager.Replace(RetrieveContact()?.ToXElement(), (contactsTree.SelectedNode.Tag as XElement));
                if(exceptionTextLabel.Text==string.Empty)// ошибок не было
                {
                    CleanForm();
                    addButton.Enabled = false;
                    saveButton.Enabled = false;
                    removeButton.Enabled = false;
                    successLabel.Text = "Изменения сохранены";
                }
            }                      
        }

        private void formField_TextChanged(object sender, EventArgs e)
        {
            addButton.Enabled = true;
        }

        private void viewFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            if (pathToPhotoTextBox.Text != string.Empty)
            {
                if (File.Exists(Path.GetFullPath(pathToPhotoTextBox.Text)))
                {
                    openDialog.FileName = Path.GetFullPath(pathToPhotoTextBox.Text);
                }
                else
                {
                    openDialog.FileName = Directory.GetCurrentDirectory();
                }
            }
            openDialog.Filter = "Image Files|*.jpg;*.bmp;*.jpeg;";
            if(openDialog.ShowDialog()==DialogResult.OK)
            {
                pathToPhotoTextBox.Text = openDialog.FileName;
                try
                {
                    photoBox.BackgroundImage = new Bitmap(openDialog.FileName);
                }
                catch
                {
                    photoBox.BackgroundImage = UI.Properties.Resources.not_found;
                }
            }            
        }       
    }
}
