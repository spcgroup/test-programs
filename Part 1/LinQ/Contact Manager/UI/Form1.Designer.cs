﻿namespace ContactManager
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.contactsTree = new System.Windows.Forms.TreeView();
            this.firstNameTextBox = new System.Windows.Forms.TextBox();
            this.isGroupedCheckBox = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.searchForm = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lastNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.homePhoneTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.mobPhoneTextBox = new System.Windows.Forms.TextBox();
            this.photoBox = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.pathToPhotoTextBox = new System.Windows.Forms.TextBox();
            this.exceptionTextLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.successLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.photoBox)).BeginInit();
            this.SuspendLayout();
            // 
            // contactsTree
            // 
            this.contactsTree.HideSelection = false;
            this.contactsTree.Location = new System.Drawing.Point(12, 123);
            this.contactsTree.Name = "contactsTree";
            this.contactsTree.Size = new System.Drawing.Size(345, 408);
            this.contactsTree.TabIndex = 2;
            this.contactsTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.contactsTree_AfterSelect);
            // 
            // firstNameTextBox
            // 
            this.firstNameTextBox.Location = new System.Drawing.Point(481, 121);
            this.firstNameTextBox.Name = "firstNameTextBox";
            this.firstNameTextBox.Size = new System.Drawing.Size(219, 20);
            this.firstNameTextBox.TabIndex = 3;
            this.firstNameTextBox.TextChanged += new System.EventHandler(this.formField_TextChanged);
            // 
            // isGroupedCheckBox
            // 
            this.isGroupedCheckBox.AutoSize = true;
            this.isGroupedCheckBox.Location = new System.Drawing.Point(13, 13);
            this.isGroupedCheckBox.Name = "isGroupedCheckBox";
            this.isGroupedCheckBox.Size = new System.Drawing.Size(96, 17);
            this.isGroupedCheckBox.TabIndex = 0;
            this.isGroupedCheckBox.Text = "Сгрупировать";
            this.isGroupedCheckBox.UseVisualStyleBackColor = true;
            this.isGroupedCheckBox.CheckedChanged += new System.EventHandler(this.isGroupedCheckBox_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(318, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Поиск";
            // 
            // searchForm
            // 
            this.searchForm.Location = new System.Drawing.Point(389, 10);
            this.searchForm.Name = "searchForm";
            this.searchForm.Size = new System.Drawing.Size(311, 20);
            this.searchForm.TabIndex = 1;
            this.searchForm.TextChanged += new System.EventHandler(this.searchForm_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(386, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(386, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Фамилия";
            // 
            // lastNameTextBox
            // 
            this.lastNameTextBox.Location = new System.Drawing.Point(481, 147);
            this.lastNameTextBox.Name = "lastNameTextBox";
            this.lastNameTextBox.Size = new System.Drawing.Size(219, 20);
            this.lastNameTextBox.TabIndex = 4;
            this.lastNameTextBox.TextChanged += new System.EventHandler(this.formField_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(386, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Группа";
            // 
            // groupTextBox
            // 
            this.groupTextBox.Location = new System.Drawing.Point(481, 173);
            this.groupTextBox.Name = "groupTextBox";
            this.groupTextBox.Size = new System.Drawing.Size(219, 20);
            this.groupTextBox.TabIndex = 5;
            this.groupTextBox.TextChanged += new System.EventHandler(this.formField_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(386, 202);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Домашний тел.";
            // 
            // homePhoneTextBox
            // 
            this.homePhoneTextBox.Location = new System.Drawing.Point(481, 199);
            this.homePhoneTextBox.Name = "homePhoneTextBox";
            this.homePhoneTextBox.Size = new System.Drawing.Size(219, 20);
            this.homePhoneTextBox.TabIndex = 6;
            this.homePhoneTextBox.TextChanged += new System.EventHandler(this.formField_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(386, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(89, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Мобильный тел.";
            // 
            // mobPhoneTextBox
            // 
            this.mobPhoneTextBox.Location = new System.Drawing.Point(481, 225);
            this.mobPhoneTextBox.Name = "mobPhoneTextBox";
            this.mobPhoneTextBox.Size = new System.Drawing.Size(219, 20);
            this.mobPhoneTextBox.TabIndex = 7;
            this.mobPhoneTextBox.TextChanged += new System.EventHandler(this.formField_TextChanged);
            // 
            // photoBox
            // 
            this.photoBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.photoBox.Location = new System.Drawing.Point(389, 265);
            this.photoBox.Name = "photoBox";
            this.photoBox.Size = new System.Drawing.Size(320, 240);
            this.photoBox.TabIndex = 14;
            this.photoBox.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(407, 249);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Фото";
            // 
            // addButton
            // 
            this.addButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.addButton.Enabled = false;
            this.addButton.Location = new System.Drawing.Point(13, 537);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(139, 40);
            this.addButton.TabIndex = 10;
            this.addButton.Text = "Добавить";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.removeButton.Enabled = false;
            this.removeButton.Location = new System.Drawing.Point(218, 537);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(139, 40);
            this.removeButton.TabIndex = 11;
            this.removeButton.Text = "Удалить";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.saveButton.Enabled = false;
            this.saveButton.Location = new System.Drawing.Point(389, 537);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(139, 40);
            this.saveButton.TabIndex = 12;
            this.saveButton.Text = "Сохранить";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(386, 514);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Путь к фото";
            // 
            // pathToPhotoTextBox
            // 
            this.pathToPhotoTextBox.Location = new System.Drawing.Point(460, 511);
            this.pathToPhotoTextBox.Name = "pathToPhotoTextBox";
            this.pathToPhotoTextBox.Size = new System.Drawing.Size(184, 20);
            this.pathToPhotoTextBox.TabIndex = 8;
            this.pathToPhotoTextBox.TextChanged += new System.EventHandler(this.formField_TextChanged);
            // 
            // exceptionTextLabel
            // 
            this.exceptionTextLabel.AutoSize = true;
            this.exceptionTextLabel.ForeColor = System.Drawing.Color.Red;
            this.exceptionTextLabel.Location = new System.Drawing.Point(104, 33);
            this.exceptionTextLabel.Name = "exceptionTextLabel";
            this.exceptionTextLabel.Size = new System.Drawing.Size(0, 13);
            this.exceptionTextLabel.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(650, 509);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(59, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Обзор...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.viewFile_Click);
            // 
            // successLabel
            // 
            this.successLabel.AutoSize = true;
            this.successLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.successLabel.ForeColor = System.Drawing.Color.Green;
            this.successLabel.Location = new System.Drawing.Point(559, 549);
            this.successLabel.Name = "successLabel";
            this.successLabel.Size = new System.Drawing.Size(0, 17);
            this.successLabel.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(731, 589);
            this.Controls.Add(this.successLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.exceptionTextLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pathToPhotoTextBox);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.photoBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.mobPhoneTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.homePhoneTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lastNameTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.searchForm);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.isGroupedCheckBox);
            this.Controls.Add(this.firstNameTextBox);
            this.Controls.Add(this.contactsTree);
            this.Name = "Form1";
            this.Text = "Contact Manager";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.photoBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView contactsTree;
        private System.Windows.Forms.TextBox firstNameTextBox;
        private System.Windows.Forms.CheckBox isGroupedCheckBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchForm;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox lastNameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox groupTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox homePhoneTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox mobPhoneTextBox;
        private System.Windows.Forms.PictureBox photoBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox pathToPhotoTextBox;
        private System.Windows.Forms.Label exceptionTextLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label successLabel;
    }
}

