﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Xml.Linq;
using System.IO;

namespace ContactManager
{
    public class Contact
    {
        public static readonly string _contactXmlString = "Contact";
        public static readonly string _firstNameXmlString = "FirstName";
        public static readonly string _lastNameXmlString = "LastName";
        public static readonly string _groupXmlString = "Group";
        public static readonly string _homePhoneXmlString = "HomePhoneNumber";
        public static readonly string _mobilePhoneXmlString = "MobilePhoneNumber";
        public static readonly string _pathToPhotoXmlString = "PathToPhoto";
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Group { get; set; }
        public string HomePhoneNumber { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string PathToPhoto { get; set; }

        //метод преобразования в объект типа XElement
        public XElement ToXElement()
        {
            return new XElement(_contactXmlString,
                        new XElement(_firstNameXmlString, FirstName),
                        new XElement(_lastNameXmlString, LastName),
                        new XElement(_groupXmlString, Group),
                        new XElement(_homePhoneXmlString, HomePhoneNumber),
                        new XElement(_mobilePhoneXmlString, MobilePhoneNumber),
                        new XElement(_pathToPhotoXmlString, PathToPhoto)
                   );
        }
    }
}
