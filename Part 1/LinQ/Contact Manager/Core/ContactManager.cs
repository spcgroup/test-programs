﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml.Linq;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;

namespace ContactManager
{
    public class ContactManager
    { 
        public XElement ContactsList { get; private set; }

        private static readonly string _ContactsListXmlString="ContactsList";

        public bool IsGrouped { get; set; }

        private string filter="";

        public delegate void ContactsChangedHandler(object collection);

        public event ContactsChangedHandler ContactsListChanged;      

        public ContactManager()
        {            
            ContactsList = new XElement(_ContactsListXmlString);           
        }

        public void Load()
        {           
            string path = ConfigurationManager.AppSettings["xmlFilePath"];
            if (File.Exists(path))
            {
                ContactsList = XElement.Load(path);
                if (filter != "")
                {
                    Filter(filter);
                }
                else
                {
                    ContactsListChanged(IsGrouped ? (object)GetGroupedContactsList() : GetOrderedContactsList());
                }
            }
        }

        public void Add(XElement item)
        {
            if (item != null)
            {
                ContactsList.Add(item);
                SaveChanges();
                if (filter != "")
                {
                    Filter(filter);
                }
                else
                {
                    ContactsListChanged(IsGrouped ? (object)GetGroupedContactsList() : GetOrderedContactsList());
                }
            }
        }

        public void Remove(XElement item)
        {
            if (item.Parent.Equals(ContactsList))
            {
                item.Remove();
                SaveChanges();
                if (filter != "")
                {
                    Filter(filter);
                }
                else
                {
                    ContactsListChanged(IsGrouped ? (object)GetGroupedContactsList() : GetOrderedContactsList());
                }
            }
        }

        public void Replace(XElement source,XElement destination)
        {
            if (source != null)
            {
                destination.Remove();
                ContactsList.Add(source);
                SaveChanges();
                if (filter != "")
                {
                    Filter(filter);
                }
                else
                {
                    ContactsListChanged(IsGrouped ? (object)GetGroupedContactsList() : GetOrderedContactsList());
                }
            }
        }

        public void SaveChanges()
        {
            ContactsList.Save(ConfigurationManager.AppSettings["xmlFilePath"]);
        }

        //сортирует список контактов по имени и фамилии
        public IEnumerable<XElement> GetOrderedContactsList()
        {
            return ContactsList.Elements()
                       .OrderBy(c => c.Element(Contact._firstNameXmlString).Value)
                       .ThenBy(c => c.Element(Contact._lastNameXmlString).Value);
        }

        //сортирует, а затем группирует список контактов
        public IEnumerable<IGrouping<string, XElement>> GetGroupedContactsList()
        {
            return ContactsList.Elements()
               .OrderBy(c => c.Element(Contact._firstNameXmlString).Value)
               .ThenBy(c => c.Element(Contact._lastNameXmlString).Value)
               .GroupBy(c => c.Element(Contact._groupXmlString).Value);
        }

        public void Filter(string name)
        {
            filter = name.Trim();
            string[] words = name.Trim().Split();
            IEnumerable<XElement> result =
                from c in ContactsList.Elements()
                from w in words
                where c.Element(Contact._firstNameXmlString).Value.ToUpper().Contains(w.ToUpper()) ||
                c.Element(Contact._lastNameXmlString).Value.ToUpper().Contains(w.ToUpper())
                orderby c.Element(Contact._firstNameXmlString).Value, c.Element(Contact._lastNameXmlString).Value
                select c;
            if (IsGrouped)
            {
                IEnumerable<IGrouping<string,XElement>> groupedResult =
                    result.GroupBy(c => c.Element(Contact._groupXmlString).Value);
                //немного не соответствует названию, т.к. передается подмножество без изменений,
                //но логика обработки та же
                ContactsListChanged(groupedResult);
            }
            else
            {
                //немного не соответствует названию, т.к. передается подмножество без изменений,
                //но логика обработки та же
                ContactsListChanged(result);
            }
                
        }

        public static void ValidateContact(Contact c)
        {
            if (c.MobilePhoneNumber == "" && c.HomePhoneNumber == "")
            {
                throw new ArgumentException("Необходимо заполнить хотя бы одно из полей 'Домашний тел.'" +
                    " или 'Мобильный тел.'" + Environment.NewLine);
            }
            string _exceptonMsg = string.Empty;//строка для сообщений об ошибках
            if (!Regex.IsMatch(c.FirstName, @"^\w(\w|[ '-]){1,19}$"))
            {           
                _exceptonMsg += "Поле 'Имя' должно содержать от 2 до 20 символов." +
                    "Допустимы буквы, цифры и символы [ '-]" + Environment.NewLine;
            }
            if (!Regex.IsMatch(c.LastName, @"^\w(\w|[ '-]){1,19}$") && c.LastName != "")
            { 
                _exceptonMsg += "Поле 'Фамилия' должно содержать от 2 до 20 символов." +
                    "Допустимы буквы, цифры и символы [ '-]" + Environment.NewLine;
            }
             if (!Regex.IsMatch(c.Group, @"^\w(\w|[ -.',+]){1,19}$")&&c.Group!="")
            {
                _exceptonMsg += "Поле 'Группа' должно содержать от 2 до 20 символов." +
                    "Допустимы буквы, цифры и символы [ -.',+]" + Environment.NewLine;
            }
            if (!Regex.IsMatch(c.HomePhoneNumber, @"^[0-9+][0-9-]{6,15}$") && c.HomePhoneNumber != "")
            {
                _exceptonMsg += "Поле 'Домашний тел.' должно содержать от 7 до 16 символов." +
                    "Допустимы только цифры и (не обязательно) знаки '-'" + Environment.NewLine;
            }
            if (!Regex.IsMatch(c.MobilePhoneNumber, @"^[0-9+][0-9-]{10,15}$") && c.MobilePhoneNumber != "")
            {           
                _exceptonMsg += "Поле 'Мобильный тел.' должно содержать от 10 до 16 символов." +
                    " Допустимы только цифры и (не обязательно) знак '-'" + Environment.NewLine;
            }
            if (c.PathToPhoto.IndexOfAny(System.IO.Path.GetInvalidPathChars()) != -1)
            {            
                _exceptonMsg += "Поле 'Путь к фото' содержит недопустимые символы" + Environment.NewLine;
            }
            if (_exceptonMsg != string.Empty)//если строка не пустая, значит были ошибки
            {
                throw new ArgumentException(_exceptonMsg);
            }
            
        }


   }
}
