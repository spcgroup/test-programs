﻿using System;
using System.Collections.Generic;

namespace Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            List<LivingThing> creatures = new List<LivingThing>
            {
                new Horse(1),
                new Crucian(2),
                new Dog(3),
                new Roach(4),
                new Horse(5),
                new Dog(6),
                new Dog(7)
            };
            Console.WriteLine($"В списке находится {creatures.Count} живых существ");
            Console.WriteLine("Общее количество ног в списке  - " +
                GetTotalLegCount(creatures).ToString());
            Console.WriteLine("Под водой могут дышать:");
            FindUnderwaterCreatures(creatures);
            Console.Read();
        }

        static int GetTotalLegCount(List<LivingThing> creatures)
        {
            int count=0;
            foreach (LivingThing i in creatures)
            {
                if(i is Animal)
                {
                    count += (i as Animal).LegCount;
                }
            }
            return count;
        }

        static void FindUnderwaterCreatures(List<LivingThing> creatures)
        {
            foreach(LivingThing item in creatures)
            {
                if(item is IUnderwater)
                {
                    Console.WriteLine("ID: "+item.LivingThingID.ToString());
                }
            }
        }
    }
}
