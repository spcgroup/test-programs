﻿namespace Inheritance
{
    interface IUnderwater
    {
        void BreatheUnderWater();
    }

    interface IVegeterian
    {
        void EatHay();
    }

}
