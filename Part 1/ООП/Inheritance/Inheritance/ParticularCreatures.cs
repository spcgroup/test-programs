﻿using System;

namespace Inheritance
{
    class Horse : Animal,IVegeterian
    {
        public Horse(int id) : base(id)
        {
            LegCount = 4;
        }

        public void EatHay()
        {
            Console.WriteLine("Я лошадь и я ем сено");
        }
    }

    class Dog:Animal,IUnderwater
    {
        public Dog(int id) : base(id)
        {
            LegCount = 4;
        }

        public void BreatheUnderWater()
        {
            Console.WriteLine("Я собака и я дышу под водой");
        }
    }

    class Crucian : Fish, IVegeterian
    {
        public Crucian(int id) : base(id) { }
        
        public override void BreatheUnderWater()
        {
            Console.WriteLine("Я карась и я дышу под водой");
        }

        public void EatHay()
        {
            Console.WriteLine("Я карась и я ем сено");
        }
    }

    class Roach : Fish
    {
        public Roach(int id) : base(id) { }

        public override void BreatheUnderWater()
        {
            Console.WriteLine("Я плотва и я дышу под водой");
        }
    }
}
