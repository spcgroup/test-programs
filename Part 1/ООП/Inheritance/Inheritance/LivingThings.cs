﻿namespace Inheritance
{
    abstract class LivingThing
    {
        public int LivingThingID { get; protected set; }
    }

    abstract class Animal:LivingThing
    {        
        public Animal(int id)
        {
            LivingThingID = id;
            LegCount = 0;
        }
        public int LegCount { get; protected set; }
    }

    abstract class Fish:LivingThing,IUnderwater
    {
        public Fish(int id)
        {
            LivingThingID = id;
        }

        public abstract void BreatheUnderWater();
    }    
}
