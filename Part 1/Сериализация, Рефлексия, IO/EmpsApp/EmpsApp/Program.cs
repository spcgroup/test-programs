﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using System;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Reflection;

namespace EmpsApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Employee> Employees = DeserializeEmployees();
            if(Employees != null)
            {
                PrintEmployees(Employees);
                Employees = new List<Employee>(Employees.Where(e => e.Age >= 25 && e.Age <= 35));
                Employees.Sort(); //order by EmployeeID
                SerializeEmployees(Employees);
            }
            Console.Read();
        }
        static List<Employee> DeserializeEmployees()
        {
            XmlRootAttribute xRoot = new XmlRootAttribute()
            {
                ElementName = "Employees"
            };
            XmlSerializer xs = new XmlSerializer(typeof(List<Employee>),xRoot);
            List<Employee> list = null;
            if (File.Exists(ConfigurationManager.AppSettings["inFile"]))
            {
                using (FileStream inStream = new FileStream(ConfigurationManager.AppSettings["InFile"], FileMode.Open))
                {
                    try
                    {
                        list = xs.Deserialize(inStream) as List<Employee>;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Failed to deserialise Employees from file");
                    }
                }
            }
            else
            {
                Console.WriteLine("Can't open file with initial xml");
            }            
            return list;
        }

        static void SerializeEmployees(List<Employee> list)
        {
            XmlRootAttribute xRoot = new XmlRootAttribute()
            {
                ElementName = "Employees"
            };
            XmlSerializer xs = new XmlSerializer(typeof(List<Employee>), xRoot);
            using (FileStream outStream = new FileStream(ConfigurationManager.AppSettings["OutFile"], FileMode.Create))
            {
                xs.Serialize(outStream, list);
            }            
        }
        static void PrintEmployees(List<Employee> list)
        {
            int i = 0;
            StringBuilder sb = new StringBuilder();
            FieldInfo idInfo = typeof(Employee).GetTypeInfo().DeclaredFields.First(f => f.Name == "EmployeeID");
            PropertyInfo addressInfo = typeof(Employee).GetProperty("Address");
            foreach (Employee item in list)
            {
                sb.AppendLine($"Employee {(++i).ToString()}");
                sb.AppendLine($"ID: {idInfo.GetValue(item) as string}");
                sb.AppendLine($"First Name: {item.FirstName}");
                sb.AppendLine($"Last Name: {item.LastName}");
                sb.AppendLine($"Age: {item.Age.ToString()}");
                sb.AppendLine($"Department: {item.Department}");
                sb.AppendLine($"Address: {addressInfo.GetValue(item) as string}");
                sb.AppendLine();
            }
            Console.WriteLine(sb.ToString());
        }
    }
}
