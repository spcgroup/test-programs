﻿using System;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;


namespace EmpsApp
{
    
    public class Employee:IComparable<Employee>,IXmlSerializable
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Department { get; set; }
        public string Address { get; set; }
        private string EmployeeID;

        public Employee(string firstName, string lastName)
        {
            EmployeeID = $"{firstName} {lastName}";
            FirstName = firstName;
            LastName = lastName;
        }

        public Employee()
        { }

        public int CompareTo(Employee other)
        {
            return EmployeeID.CompareTo(other.EmployeeID);
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            reader.ReadStartElement();
            FirstName = reader.ReadElementContentAsString("FirstName", "");
            LastName = reader.ReadElementContentAsString("LastName", "");
            Age = reader.ReadElementContentAsInt("Age", "");
            Department = reader.ReadElementContentAsString("Department", "");
            Address = reader.ReadElementContentAsString("Address", "");
            reader.ReadEndElement();            
            EmployeeID = $"{FirstName} {LastName}";
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteElementString("FirstName", FirstName);
            writer.WriteElementString("LastName", LastName);
            writer.WriteStartElement("Age");
            writer.WriteValue(Age);
            writer.WriteEndElement();
            writer.WriteElementString("Department", Department);
            writer.WriteElementString("Address", Address);            
        }
    }
}
