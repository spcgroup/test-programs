﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Counters
{
    public partial class Default : System.Web.UI.Page
    {
        public IEnumerable<ViewCounter> GetPagesToObserve()
        {
            return (IEnumerable<ViewCounter>)Application[CountersHelper.PAGE_COUNTERS_LIST];
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Application.Lock();
            appRequestedTotal.InnerText = 
                $"Запросов к приложению: {(Application[CountersHelper.APP_TOTAL] as ViewCounter).Count}";
            appRequestedToday.InnerText =
                $"Запросов к приложению (сегодня): {(Application[CountersHelper.APP_TODAY] as ViewCounter).Count}";
            uniqueGuests.InnerText =
                $"Уникальных посетителей: {(Application[CountersHelper.GUESTS_UNIQUE] as ViewCounter).Count}";
            guestsToday.InnerText =
                $"Посетителей за сегодня: {(Application[CountersHelper.GUESTS_TODAY] as ViewCounter).Count}";
            Application.UnLock();
        }
    }
}