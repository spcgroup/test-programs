﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Counters
{
    public class UserIdentityInfo
    {
        public string UserAgent { get; set; }
        public string UserAdress { get; set; }
    }
}