﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Counters
{
    [Serializable]
    public class ViewCounter
    {
        public string Target { get; set; }
        public int Count { get;private set; }
        public DateTime CurrentDate { get; set; }

        public int Increment()
        {
            return ++Count;
        }

        public void Reset()
        {
            Count = 0;
        }
    }
}