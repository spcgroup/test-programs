﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Counters.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h2>Statistics:</h2>
        <p id="appRequestedTotal" runat="server"></p>
        <p id="appRequestedToday" runat="server"></p>
        <p id="guestsToday" runat="server"></p>
        <p id="uniqueGuests" runat="server"></p>
        <asp:Repeater ID="pagesRepeater" ItemType="Counters.ViewCounter" 
            SelectMethod="GetPagesToObserve" runat="server">
            <ItemTemplate>              
                    <div><a href="<%#: Item.Target %>"><%#: Item.Target %></a><span> viewed <%#: Item.Count %> times</span></div>                    
            </ItemTemplate>
        </asp:Repeater>
    </div>
    </form>
</body>
</html>
