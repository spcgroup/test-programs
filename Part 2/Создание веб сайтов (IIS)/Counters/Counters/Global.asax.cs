﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Configuration;
using System.IO;

namespace Counters
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            HashSet<UserIdentityInfo> uniqueGuests = new HashSet<UserIdentityInfo>();
            Application[CountersHelper.GUESTS_INFO_LIST] = uniqueGuests;
            List<ViewCounter> pageCounters = new List<ViewCounter>();
            if (File.Exists(WebConfigurationManager.AppSettings[CountersHelper.CONFIG_PAGES_FILE]))
            {
                foreach (string path in File.ReadAllLines(WebConfigurationManager.AppSettings[CountersHelper.CONFIG_PAGES_FILE]))
                {
                    pageCounters.Add(new ViewCounter()
                    {
                        Target = path
                    });
                }
                Application[CountersHelper.PAGE_COUNTERS_LIST] = pageCounters;
            }
            Application[CountersHelper.APP_TOTAL] = new ViewCounter();
            Application[CountersHelper.APP_TODAY] = new ViewCounter()
            {
                CurrentDate = DateTime.Now.Date
            };
            Application[CountersHelper.GUESTS_TODAY] = new ViewCounter()
            {
                CurrentDate = DateTime.Now.Date
            };
            Application[CountersHelper.GUESTS_UNIQUE] = new ViewCounter();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            Application.Lock();
            if ((Application[CountersHelper.GUESTS_TODAY] as ViewCounter).CurrentDate != DateTime.Now.Date)
            {
                (Application[CountersHelper.GUESTS_TODAY] as ViewCounter).Reset();
            }
            (Application[CountersHelper.GUESTS_TODAY] as ViewCounter).Increment();
            if((Application[CountersHelper.GUESTS_INFO_LIST] as HashSet<UserIdentityInfo>)
                .Where(uii => uii.UserAdress == Request.UserHostAddress && uii.UserAgent == Request.UserAgent)
                .FirstOrDefault() == null)
            {
                (Application[CountersHelper.GUESTS_INFO_LIST] as HashSet<UserIdentityInfo>).Add(new UserIdentityInfo()
                {
                    UserAgent = Request.UserAgent,
                    UserAdress = Request.UserHostAddress
                });
                (Application[CountersHelper.GUESTS_UNIQUE] as ViewCounter).Increment();
            }
            Application.UnLock();
        }

        protected void Application_BeginRequest(object sender,EventArgs e)
        {
            string page = Request.FilePath;
            Application.Lock();
            (Application[CountersHelper.APP_TOTAL] as ViewCounter).Increment();
            if((Application[CountersHelper.APP_TODAY] as ViewCounter).CurrentDate != DateTime.Now.Date)
            {
                (Application[CountersHelper.APP_TODAY] as ViewCounter).Reset();
            }
            (Application[CountersHelper.APP_TODAY] as ViewCounter).Increment();
            (Application[CountersHelper.PAGE_COUNTERS_LIST] as List<ViewCounter>)
                .Where(c => page == c.Target)
                .FirstOrDefault()?.Increment();
            Application.UnLock();
        }
    }
}