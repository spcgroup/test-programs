﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Counters
{
    public class CountersHelper
    {
        public const string CONFIG_PAGES_FILE = "pagesFile";
        public const string PAGE_COUNTERS_LIST = "pageCounters";
        public const string APP_TOTAL = "appTotal";
        public const string APP_TODAY = "appToday";
        public const string GUESTS_TODAY = "guestsToday";
        public const string GUESTS_UNIQUE = "uniqueGuests";
        public const string GUESTS_INFO_LIST = "guestsList";
    }
}