﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApp1_DeploymentUtility
{
    class AppConfigHelper
    {
        public const string USER_NAME = "userName";
        public const string PASSWORD = "password";
        public const string SOURCE_FOLDER = "sourceFolderName";
        public const string DEST_ROOT = "destRoot";
        public const string DEST_FOLDER = "destFolder";
        public const string POOL_NAME = "appPoolName";
        public const string SITE_NAME = "siteName";
    }
}
