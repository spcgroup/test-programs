﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Microsoft.Web.Administration;
using System.Net.NetworkInformation;
using System.Configuration;

namespace WebApp1_DeploymentUtility
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                using (ServerManager serverManager = new ServerManager())
                {
                    if(serverManager.ApplicationPools[ConfigurationManager.AppSettings[AppConfigHelper.POOL_NAME]]== null &&
                        serverManager.Sites[ConfigurationManager.AppSettings[AppConfigHelper.SITE_NAME]] == null)
                    {
                        
                        string targetPath ="";
                        if(Directory.Exists(ConfigurationManager.AppSettings[AppConfigHelper.DEST_ROOT]))
                        {
                            targetPath = Path.Combine(ConfigurationManager.AppSettings[AppConfigHelper.DEST_ROOT],
                                ConfigurationManager.AppSettings[AppConfigHelper.DEST_FOLDER]);
                        }
                        else
                        {
                            targetPath = Path.Combine(Directory.GetCurrentDirectory(),
                                ConfigurationManager.AppSettings[AppConfigHelper.DEST_FOLDER]);
                        }
                        string webFilesPath = Path.Combine(Directory.GetCurrentDirectory(),
                            ConfigurationManager.AppSettings[AppConfigHelper.SOURCE_FOLDER]);
                        CopyDirectory(webFilesPath, targetPath);                       
                        ApplicationPool appPool = serverManager.ApplicationPools
                            .Add(ConfigurationManager.AppSettings[AppConfigHelper.POOL_NAME]);
                        appPool.ProcessModel.IdentityType = ProcessModelIdentityType.SpecificUser;
                        appPool.ProcessModel.UserName = ConfigurationManager.AppSettings[AppConfigHelper.USER_NAME];
                        appPool.ProcessModel.Password = ConfigurationManager.AppSettings[AppConfigHelper.PASSWORD];
                        Site site = serverManager.Sites.Add(ConfigurationManager.AppSettings[AppConfigHelper.SITE_NAME]
                            , targetPath, GetFreePort());
                        site.ApplicationDefaults.ApplicationPoolName = appPool.Name;
                        site.Applications[0].VirtualDirectories[0].UserName =
                            ConfigurationManager.AppSettings[AppConfigHelper.USER_NAME];
                        site.Applications[0].VirtualDirectories[0].Password =
                            ConfigurationManager.AppSettings[AppConfigHelper.PASSWORD];
                        serverManager.CommitChanges();
                        Console.WriteLine("Сайт успешно развернут");
                    }
                    else
                    {
                        Console.WriteLine("ApplicationPool или Site с заданным именем уже существует");
                    }
                }
            }
            catch(UnauthorizedAccessException)
            {
                Console.WriteLine("Ошибка! Запустите программу от имени администратора");
            }
            catch(Exception ex)
            {
                Console.WriteLine($"Ошибка! {ex.Message}");
            }
            Console.Read();
        }

        static int GetFreePort()
        {
            int[] busyPorts = IPGlobalProperties.GetIPGlobalProperties().GetActiveTcpConnections()
                .Select(p => p.LocalEndPoint.Port).ToArray();
            Random r = new Random();
            int port = 0;
            while(busyPorts.Contains(port = r.Next(1025, 65535)))
            { }
            return port;    
        }

        static void CopyDirectory(string sourcePath, string destPath)
        {
            if (!Directory.Exists(destPath))
            {
                Directory.CreateDirectory(destPath);
            }
            foreach (string dir in Directory.EnumerateDirectories(sourcePath))
            {
                CopyDirectory(dir, Path.Combine(destPath, new DirectoryInfo(dir).Name));
            }
            foreach (string file in Directory.EnumerateFiles(sourcePath))
            {
                string destFile = Path.Combine(destPath, new FileInfo(file).Name);
                if(File.Exists(destFile))
                {
                    File.Delete(destFile);
                }
                File.Copy(file, destFile);
            }
        }
    }
}
