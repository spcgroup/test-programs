﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApp1
{
    public partial class Default : System.Web.UI.Page
    {
        //в коллекции Request.Form хранятся данные, полученные из веб формы при  POST запросе
        //когда клиент запрашивает страницу сервер получает GET запрос, поэтому коллекция Request.Form пустая,
        //а когда пользователь нажимает на кнопку, сервер получает POST запрос, и в Request.Form уже находятся
        //данные введенные пользователем в форме и ViewState
        private void BuildFormItemsTable()
        {            
            Response.Write("<table><tr><th>Name</th><th>Value</th></tr>");
            foreach (string name in Request.Form.AllKeys)
            {
                Response.Write($"<tr><td>{name}</td><td>{Request.Form[name]}</td><tr>");
            }
            Response.Write("</table>");
        }
                
        protected void Page_PreInit(object src, EventArgs args)
        {            
            Response.Write($"Event: Page_PreInit {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_Init(object src, EventArgs args)
        {            
            Response.Write($"Event: Page_Init {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_InitComplete(object src, EventArgs args)
        {
            Response.Write($"Event: Page_InitComplete {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_PreLoad(object src, EventArgs args)
        {
            Response.Write($"Event: Page_PreLoad {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_Load(object src, EventArgs args)
        {
            Response.Write($"Event: Page_Load {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_LoadComplete(object src, EventArgs args)
        {
            Response.Write($"Event: Page_LoadComplete {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_PreRender(object src, EventArgs args)
        {
            Response.Write($"Event: Page_PreRender {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_PreRenderComplete(object src, EventArgs args)
        {
            Response.Write($"Event: Page_PreRenderComplete {DateTime.Now.ToString()}<br/>");
        }

        protected void Page_SaveStateComplete(object src, EventArgs args)
        {
            Response.Write($"Event: Page_SaveStateComplete {DateTime.Now.ToString()}<br/>");
        }

        //момент вызова этого события невозможно зафиксировать через Page.Response потому, что на момент его возникновения
        //объект Page.Response уже не существует
        protected void Page_Unload(object src, EventArgs args)
        {
            
        }

        //моменты вызова методов ProcessRequest, DeterminePostBackMode, LoadPageStateFromPersistenceMedium
        //OnInit, OnInitComplete, ProcessPostData, Validate, SavePageStateToPersistenceMedium, RenderControl, RenderChildren
        //OnLoad, OnPreRender, OnUnload и Dispose невозможно зафиксировать потому, что они объявлены как не виртуальные

        //момент вызова этого метода невозможно зафиксировать через Page.Response, потому что на момент его вызова
        //объект Page.Response еще не создан
        protected override void Construct()
        {            
            base.Construct();
        }

        protected override void InitializeCulture()
        {
            Response.Write($"Method: InitializeCulture {DateTime.Now.ToString()}<br/>");
            base.InitializeCulture();
        }

        protected override void OnPreInit(EventArgs e)
        {
            Response.Write($"Method: OnPreInit {DateTime.Now.ToString()}<br/>");
            base.OnPreInit(e);
        }
        
        protected override void TrackViewState()
        {
            Response.Write($"Method: TrackViewState {DateTime.Now.ToString()}<br/>");
            base.TrackViewState();
        }

        protected override void LoadViewState(object savedState)
        {
            Response.Write($"Method: LoadViewState {DateTime.Now.ToString()}<br/>");
            base.LoadViewState(savedState);
        }

        protected override void OnPreLoad(EventArgs e)
        {
            Response.Write($"Method: OnPreLoad {DateTime.Now.ToString()}<br/>");
            base.OnPreLoad(e);
        }

        protected override void RaisePostBackEvent(IPostBackEventHandler sourceControl, string eventArgument)
        {
            Response.Write($"Method: RaisePostBackEvent {DateTime.Now.ToString()}<br/>");
            base.RaisePostBackEvent(sourceControl, eventArgument);
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            Response.Write($"Method: OnLoadComplete {DateTime.Now.ToString()}<br/>");
            base.OnLoadComplete(e);
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            Response.Write($"Method: OnPreRenderComplete {DateTime.Now.ToString()}<br/>");
            base.OnPreRenderComplete(e);
        }

        protected override object SaveViewState()
        {
            Response.Write($"Method: SaveViewState {DateTime.Now.ToString()}<br/>");
            return  base.SaveViewState();
        }

        protected override void OnSaveStateComplete(EventArgs e)
        {
            Response.Write($"Method: OnSaveStateComplete {DateTime.Now.ToString()}<br/>");
            base.OnSaveStateComplete(e);
        }

        protected override void Render(HtmlTextWriter writer)
        {
            Response.Write($"Method: Render {DateTime.Now.ToString()}<br/>");
            BuildFormItemsTable();
            base.Render(writer);
        }

        protected override void OnInitComplete(EventArgs e)
        {
            Response.Write($"Method: OnInitComplete {DateTime.Now.ToString()}<br/>");
            base.OnInitComplete(e);
        }
    }
}