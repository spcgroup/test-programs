﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProductManagement.aspx.cs" Inherits="ProductRotator.ProductManagement" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <link rel="stylesheet" href="css/manager styles.css" />
</head>
<body>
    <a href="index.html">View</a>
    <form id="form1" runat="server">
        <div>
            <asp:ListView ItemType="ProductRotator.Models.Product" runat="server"
                SelectMethod="GetProducts" UpdateMethod="UpdateProduct"
                DeleteMethod="DeleteProduct" DataKeyNames="ProductID"
                InsertMethod="InsertProduct" InsertItemPosition="LastItem">
                <LayoutTemplate>
                    <table>
                        <tr>
                            <th>Id</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>ImageURL</th>
                        </tr>
                        <tr id="itemPlaceholder" runat="server"></tr>
                    </table>
                    <tr>
                        <td colspan="5">
                             <asp:DataPager PageSize="6" runat="server"> 
                                 <Fields>
                                     <asp:NextPreviousPagerField ButtonType="Button"
                                          ShowFirstPageButton="true" ShowPreviousPageButton="true" 
                                          ShowNextPageButton="true" ShowLastPageButton="true"/>
                                 </Fields>
                             </asp:DataPager> 
                        </td>
                    </tr>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%#: Item.ProductID %></td>
                        <td><%#: Item.Description%></td>
                        <td><%#: Item.Price %></td>
                        <td><%#: Item.ImageURL %></td>
                        <td>
                            <asp:Button CommandName="Edit" Text="Edit" runat="server" />
                            <asp:Button CommandName="Delete" Text="Delete" runat="server" />
                        </td>
                    </tr>
                </ItemTemplate>
                <EditItemTemplate>
                    <tr>
                        <td><%#: Item.ProductID %></td>
                        <td>
                            <input type="text" id="description" runat="server" value="<%#: BindItem.Description%>" /></td>
                        <td>
                            <input type="text" id="price" runat="server" value="<%#: BindItem.Price %>" /></td>
                        <td>
                            <input type="text" id="imageUrl" runat="server" value="<%#: BindItem.ImageURL %>" /></td>
                        <td>
                            <asp:Button CommandName="Update" Text="Update" runat="server" />
                            <asp:Button CommandName="Cancel" Text="Cancel" runat="server" />
                        </td>
                    </tr>
                    
                </EditItemTemplate>
                <EmptyDataTemplate>
                    Can't find any data
                </EmptyDataTemplate>
                <InsertItemTemplate>
                    <tr>
                        <td>New record:</td>
                        <td>
                            <input type="text" id="description" runat="server" value="<%#: BindItem.Description%>" /></td>
                        <td>
                            <input type="text" id="price" runat="server" value="<%#: BindItem.Price %>" /></td>
                        <td>
                            <input type="text" id="imageUrl" runat="server" value="<%#: BindItem.ImageURL %>" /></td>
                        <td>
                            <asp:Button CommandName="Insert" Text="Insert" runat="server" /></td>
                    </tr>
                    <tr>
                        <td class="error" colspan="5">
                            <asp:ValidationSummary runat="server" />
                        </td>
                    </tr>
                </InsertItemTemplate>
            </asp:ListView>
        </div>
    </form>
</body>
</html>
