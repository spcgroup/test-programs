﻿using System;
using System.Linq;
using System.Web;
using ProductRotator.Models;

namespace ProductRotator
{
    public class ProductJsonHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            int pageIndex = 1;
            int itemCount = 3;
            int.TryParse(request["page"], out pageIndex);
            int.TryParse(request["count"], out itemCount);

            response.ContentType = "application/json";
            ProductRepository repo = new ProductRepository();
            Product[] products = repo.Products
                .OrderBy(p => p.ProductID)
                .Skip((pageIndex - 1) * itemCount)
                .Take(itemCount).ToArray();

            string jsonString = "[ ";
            foreach (Product item in products)
            {
                string curItemJson =
                    $"{{ \"{ProductRotatorHelper.ImageURL}\":\"{item.ImageURL}\"" +
                    $",\"{ProductRotatorHelper.Description}\":\"{item.Description}\"" +
                    $",\"{ProductRotatorHelper.Price}\": \"{item.Price}\" }},";
                jsonString += curItemJson;
            }
            jsonString += $"{{ \"IsLastPage\":\"{products.Last() == repo.Products.OrderBy(p => p.ProductID).Last()}\"}}";
            jsonString += " ]";

            response.Clear();
            response.Write(jsonString);
            response.End();
        }
    }
}
