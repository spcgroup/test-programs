namespace ProductRotator.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProductID { get; set; }

        [Required]
        [StringLength(255)]
        public string ImageURL { get; set; }

        [Required]
        [StringLength(500)]
        public string Description { get; set; }

        
        [Column(TypeName = "money")]
        [RegularExpression(@"[^-]*", ErrorMessage = "Price cannot be less than 0")]
        public decimal Price { get; set; }
    }
}
