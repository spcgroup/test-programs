﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace ProductRotator.Models
{
    public class ProductRepository
    {
        public List<Product> Products { get; set; }
        
        public ProductRepository()
        {
            using(ProductContext context = new ProductContext())
            {
                Products = context.Products.ToList();
            }
        }


        public Product GetProduct(int ProductID)
        {
            return Products.Where(p => p.ProductID == ProductID).FirstOrDefault();
        }

        public void AddProduct(Product product)
        {
            using (ProductContext context = new ProductContext())
            {
                product.ProductID = context.Products.Max(p => p.ProductID) + 1;
                context.Products.Add(product);
                context.SaveChanges();
            }
        }

        public void UpdateProduct(Product product)
        {
            using (ProductContext context = new ProductContext())
            {
                Product prod = context.Products.Where(p => p.ProductID == product.ProductID).FirstOrDefault();
                prod.Description = product.Description;
                prod.Price = product.Price;
                prod.ImageURL = product.ImageURL;
                context.SaveChanges();
            }
        }

        public void DeleteProduct(int ProductID)
        {
            using (ProductContext context = new ProductContext())
            {
                Product prod = context.Products.Where(p => p.ProductID == ProductID).FirstOrDefault();
                if(prod != null)
                {
                    context.Products.Remove(prod);
                    context.SaveChanges();
                }
            }
        }
    }
}