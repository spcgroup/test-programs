﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProductRotator
{
    public class ProductRotatorHelper
    {
        public const string ProductID = "ProductID";
        public const string Description = "Description";
        public const string ImageURL = "ImageURL";
        public const string Price = "Price";
    }
}