﻿(function ($) {
    var $itemTemplate = $("<div class='item'>" +
            "<img class='item-image' />" +
            "<span class='item-descr'></span>" +
            "<span class='item-price'></span></div>");
    var page = 1;
    var count = 3;
    var loadProducts = function () {
        $.get("product.json", { "page": page, "count": count }, productsLoaded);
    }

    $(document).ready(function () {
        loadProducts();
        $("#next").on("click", function (e) {
            page += 1;
            loadProducts();
            e.preventDefault();
        });
        $("#prev").on("click", function (e) {
            page -= 1;
            loadProducts();
            e.preventDefault();
        });
    });

    var productsLoaded = function (data) {
        $("#content").empty();
        if (data.length - 1) {
            $("#next").prop("disabled", data[data.length -1].IsLastPage == "True");
            for (var i = 0; i < data.length - 1; i++) {
                var $curItem = $itemTemplate.clone();
                $curItem.find(".item-image").attr("src", data[i].ImageURL);
                $curItem.find(".item-image").attr("alt", "Фото не найдено");
                $curItem.find(".item-descr").text(data[i].Description);
                $curItem.find(".item-descr").attr("title", data[i].Description);
                $curItem.find(".item-price").text(parseFloat(data[i].Price.replace(",",".")).toFixed(2) + " грн");
                $("#content").append($curItem);
            }
            $("#prev").prop("disabled", page <= 1);
        }
        else {
            $("#next").prop("disabled", true);
        }
    };
})(jQuery);