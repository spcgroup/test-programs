﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ProductRotator.Models;

namespace ProductRotator
{
    public partial class ProductManagement : System.Web.UI.Page
    {
        public IQueryable<Product> GetProducts()
        {
            return new ProductRepository().Products.AsQueryable();
        }

        public void UpdateProduct(int? ProductID)
        {
            ProductRepository rep = new ProductRepository();
            Product p = rep.GetProduct((int)ProductID);
            if(p != null && TryUpdateModel(p))
            {
                rep.UpdateProduct(p);
            }
        }

        public void DeleteProduct(int? ProductID)
        {
            new ProductRepository().DeleteProduct((int)ProductID);
        }

        public void InsertProduct()
        {
            Product p = new Product();
            if(TryUpdateModel(p))
            {
                new ProductRepository().AddProduct(p);
            }
        }
    }
}