﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="Watermarks.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        input{
            margin:10px;
        }
    </style>
</head>
<body>
    <form id="form1" method="post" enctype="multipart/form-data" runat="server">
    <div>
        <a id="homeLink" runat="server">Home</a>
        <input type="file" id="fileToUpload" runat="server"/>
    </div>
        <input type="submit" id="uploadButton" value="Upload" onserverclick="uploadButton_ServerClick" runat="server" />
        <p id="output" runat="server"></p>
    </form>
</body>
</html>
