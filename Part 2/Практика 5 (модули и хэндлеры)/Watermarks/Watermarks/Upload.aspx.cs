﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Watermarks
{
    public partial class Upload : System.Web.UI.Page
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            homeLink.HRef=$"/Home.aspx?{WatermarksHelper.QUERY_USER_KEY}=" +
                    $"{Request.QueryString[WatermarksHelper.QUERY_USER_KEY]}";
            output.InnerText = "";
        }

        protected void uploadButton_ServerClick(object sender, EventArgs e)
        {
            string[] validExtensions = { ".jpg", ".jpeg", ".bmp", ".png", ".gif" };
            if ((fileToUpload.PostedFile != null) && (fileToUpload.PostedFile.ContentLength > 0) &&
                validExtensions.Contains(Path.GetExtension(fileToUpload.PostedFile.FileName)))
            {

                Application.Lock();
                Application[WatermarksHelper.LAST_FILE_ID] = (Application[WatermarksHelper.LAST_FILE_ID] as int?) + 1;
                string fileID = (Application[WatermarksHelper.LAST_FILE_ID] as int?).ToString();
                Application.UnLock();
                string SaveLocation = Server.MapPath(WatermarksHelper.IMAGE_FOLDER) + "\\" + fileID +
                    Path.GetExtension(fileToUpload.PostedFile.FileName);
                try
                {
                    fileToUpload.PostedFile.SaveAs(SaveLocation);
                    output.InnerText = $"Your image has been uploaded. ID: {fileID}";
                }
                catch
                {
                    output.InnerText = "Error! The file has not been uploaded.";
                }
            }
            else
            {
                output.InnerText = "Please select an image file to upload.";
            }
        }
    }
}