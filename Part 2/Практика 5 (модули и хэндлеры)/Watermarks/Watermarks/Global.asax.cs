﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.IO;

namespace Watermarks
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            ImagesManager imagesManager = new ImagesManager(Server.MapPath(WatermarksHelper.IMAGE_FOLDER));
            Application[WatermarksHelper.IMAGES_MANAGER] = imagesManager;
            Application[WatermarksHelper.LAST_FILE_ID] = imagesManager.GetLastImageID();      
        }
    }
}