﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace Watermarks
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                uploadLink.HRef = $"/Upload.aspx?{WatermarksHelper.QUERY_USER_KEY}=" +
                    $"{Request.QueryString[WatermarksHelper.QUERY_USER_KEY]}";
                foreach (string item in (Application[WatermarksHelper.IMAGES_MANAGER] as ImagesManager).GetImages().ToList())
                {
                    imagesList.Items.Add(Path.GetFileName(item));
                }
            }
        }

        protected void showImageButton_ServerClick(object sender, EventArgs e)
        {
            if(imagesList.SelectedIndex!= -1)
            {
                
                string imageUrl=$"/{WatermarksHelper.SELECTED_IMAGE_URL}?" +
                    $"{WatermarksHelper.QUERY_USER_KEY}={Request.QueryString[WatermarksHelper.QUERY_USER_KEY]}" +
                    $"&{WatermarksHelper.QUERY_IMAGE_ID}={imagesList.SelectedValue}";
                ClientScript.RegisterStartupScript(Page.GetType(), "open_window",
                                $"void(window.open('{imageUrl}', 'child_window'));", true);
            }
            else
            {
                Response.Write("Select an image first");
            }
        }
    }
}