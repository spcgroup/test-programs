﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;

namespace Watermarks
{
    public class ImageHandler : IHttpHandler
    {
        public bool IsReusable
        {
            get {return false; }            
        }

        public void ProcessRequest(HttpContext context)
        {
            HttpResponse response = context.Response;
            HttpRequest request = context.Request;
            HttpApplicationState application = context.Application;
            if ((request.QueryString[WatermarksHelper.QUERY_IMAGE_ID] ?? "") != "")
            {
                response.ContentType = "image/jpeg";
                ImagesManager imageManager = application[WatermarksHelper.IMAGES_MANAGER] as ImagesManager;
                System.Drawing.Image img =
                   imageManager.GetImageWithWatermark(request.QueryString[WatermarksHelper.QUERY_IMAGE_ID]);
                if (img != null)
                {
                    img.Save(response.OutputStream, ImageFormat.Jpeg);
                }
                else
                {
                    response.ContentType = "text/html";
                    response.Write($"Image with ID:{request.QueryString[WatermarksHelper.QUERY_IMAGE_ID]} not found");
                }
            }
            else
            {
                response.Write($"Wrong request format (QueryString parameter {WatermarksHelper.QUERY_IMAGE_ID} is not set)");
            }
        }
    }
}
