﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Drawing;
using System.Threading.Tasks;

namespace Watermarks
{
    public class ImagesManager
    {
        private string _folder;
        
        public ImagesManager(string folder)
        {
            _folder = folder;
        }
              
        public IEnumerable<string> GetImages()
        {
            return Directory.EnumerateFiles(_folder)
                .Select(p => Path.GetFileName(p))
                .Select(p => p.Remove(p.LastIndexOf('.')))
                .Where(p => 
                {
                    int temp=0;
                    return int.TryParse(p,out temp);
                });
        }
        
        public int GetLastImageID()
        {
            List<string> images = GetImages().ToList();
            if (images.Count() > 0)
            {
                return images.Select(p => int.Parse(p)).Max();
            }
            else
            {
                return 0;
            }
        }

        public Image GetImageWithWatermark(string ID)
        {
            string path = Directory.EnumerateFiles(_folder)
                                   .Where(p => Path.GetFileName(p)
                                   .StartsWith(ID + "."))
                                   .FirstOrDefault();
            if (path != null)
            {
                string watermark = WebConfigurationManager.AppSettings[WatermarksHelper.CONFIG_WATERMARK_KEY];
                Image img = Image.FromFile(path);
                Graphics graphics = Graphics.FromImage(img);
                float fontHeight = (float)(img.Width / 10.0) ;
                Font font = new Font("Courier", fontHeight, FontStyle.Bold | FontStyle.Italic,GraphicsUnit.Point);
                SolidBrush brush = new SolidBrush(Color.FromArgb(0x66, Color.White));
                float xPosition = (float)((img.Width - watermark.Length * font.SizeInPoints/1.5) / 2.0);
                float yPosition = (float)((img.Height - fontHeight) / 2.0);
                graphics.DrawString("Watermark", font, brush, xPosition, yPosition);
                graphics.Save();
                return img;
            }
            else
            {
                return null;
            }
        }
    }
}