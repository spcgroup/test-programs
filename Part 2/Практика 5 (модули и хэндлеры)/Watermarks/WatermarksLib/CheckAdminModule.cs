﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Security.Cryptography;

namespace Watermarks
{
    class CheckAdminModule : IHttpModule
    {
        public void Dispose()
        {

        }

        public void Init(HttpApplication context)
        {            
            context.BeginRequest += (sender, e) =>
            {  
                string adminKey = GetMd5Hash(WatermarksHelper.USER_KEY_SOURCE);
                if ((context.Request.QueryString[WatermarksHelper.QUERY_USER_KEY] ?? "") != adminKey)
                {
                    context.Response.Write("<h1>You Are Not Autorized!</h1>");
                    context.Response.End();
                }                
            };
        }

        private static string GetMd5Hash(string input)
        {
            MD5 md5hash = MD5.Create();
            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

    }
}
