﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Watermarks
{
    public class WatermarksHelper
    {
        public const string IMAGE_FOLDER = "Images";
        public const string LAST_FILE_ID = "lastFileID";
        public const string IMAGES_MANAGER = "imagesManager";
        public const string CONFIG_WATERMARK_KEY = "watermark";
        public const string QUERY_USER_KEY = "UserKey";
        public const string QUERY_IMAGE_ID = "ImageID";
        public const string SELECTED_IMAGE_URL = "SelectedImage";
        public const string USER_KEY_SOURCE = "I_Am_Real_Admin";
    }
}