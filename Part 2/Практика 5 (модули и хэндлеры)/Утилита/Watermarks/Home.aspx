﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Watermarks.Home" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        div{
            margin: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div><a id="uploadLink" runat="server">Upload image</a></div>        
        <div><asp:DropDownList ID="imagesList" runat="server" /></div>
        <div><input type="button" id="showImageButton" value="Show" onserverclick="showImageButton_ServerClick" runat="server"/></div>
    </div>
    </form>
</body>
</html>
