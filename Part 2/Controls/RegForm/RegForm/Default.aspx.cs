﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace RegForm
{
    public partial class Default : System.Web.UI.Page
    {
        List<string> countries = new List<string>()
        {
            "Украина", "Россия", "Беларусь",
            "Польша", "Германия", "США"
        };

        Dictionary<string, string> cities = new Dictionary<string, string>()
        {
            {"Киев", "Украина" }, {"Харьков", "Украина" },
            {"Москва", "Россия" }, {"Санкт-Петербург", "Россия" },
            {"Варшава", "Польша" }, {"Краков", "Польша" },
            {"Минск", "Беларусь" }, {"Гомель", "Беларусь" },
            {"Берлин", "Германия" }, {"Франкфурт", "Германия" },
            {"Нью-Йорк", "США" }, {"Лос-Анжелес", "США" }
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            HtmlGenericControl contentDiv = FindControl("content") as HtmlGenericControl;
            HtmlTable table = new HtmlTable();
            contentDiv.Controls.Add(table);

            HtmlTableRow curRow = MakeRow("Псевдоним пользователя");
            curRow.Cells.Add(MakeInputCell(RegFormHelper.NICKNAME_ID));
            table.Rows.Add(curRow);
            contentDiv.Controls.Add(MakeRequiredFieldValidator("Псевдоним пользователя", RegFormHelper.NICKNAME_ID));

            curRow = MakeRow("Имя");
            curRow.Cells.Add(MakeInputCell(RegFormHelper.FIRST_NAME_ID));
            table.Rows.Add(curRow);
            contentDiv.Controls.Add(MakeRequiredFieldValidator("Имя", RegFormHelper.FIRST_NAME_ID));
            contentDiv.Controls.Add(MakeNotEqualValidator(RegFormHelper.FIRST_NAME_ID, RegFormHelper.NICKNAME_ID,
                "Значение поля 'Имя' должно отличаться от 'Псевдоним пользователя'"));

            curRow = MakeRow("Фамилия");
            curRow.Cells.Add(MakeInputCell(RegFormHelper.LAST_NAME_ID));
            table.Rows.Add(curRow);
            contentDiv.Controls.Add(MakeRequiredFieldValidator("Фамилия", RegFormHelper.LAST_NAME_ID));
            contentDiv.Controls.Add(MakeNotEqualValidator(RegFormHelper.LAST_NAME_ID, RegFormHelper.FIRST_NAME_ID,
                "Значение поля 'Фамилия' должно отличаться от 'Имя'"));

            curRow = MakeRow("Дата рождения");
            curRow.Cells.Add(MakeCalendarCell());
            table.Rows.Add(curRow);
            contentDiv.Controls.Add(MakeRequiredFieldValidator("Дата рождения", RegFormHelper.BIRTH_DATE_ID));

            curRow = MakeRow("E-mail");
            curRow.Cells.Add(MakeInputCell(RegFormHelper.EMAIL_ID));
            table.Rows.Add(curRow);
            contentDiv.Controls.Add(MakeRequiredFieldValidator("E-mail", RegFormHelper.EMAIL_ID));

            RegularExpressionValidator regExpVal = new RegularExpressionValidator();
            regExpVal.ControlToValidate = (FindControl(RegFormHelper.EMAIL_ID) as TextBox).ID;
            regExpVal.Display = ValidatorDisplay.None;
            regExpVal.EnableClientScript = false;
            regExpVal.ErrorMessage = "Поле 'E-mail' должно содержать правильный адрес электронной почты";
            regExpVal.ValidationExpression = @"[a-zA-Z_0-9.-]+\@[a-zA-Z_0-9.-]+\.\w+";
            contentDiv.Controls.Add(regExpVal);        

            curRow = MakeRow("Страна");
            curRow.Cells.Add(MakeRequiredCountriesCell());
            table.Rows.Add(curRow);
            contentDiv.Controls.Add(MakeRequiredFieldValidator("Страна", RegFormHelper.COUNTRY_ID));

            curRow = MakeRow("Город");
            curRow.Cells.Add(MakeRequiredCitiesCell());
            table.Rows.Add(curRow);
            contentDiv.Controls.Add(MakeRequiredFieldValidator("Город", RegFormHelper.COUNTRY_ID));

            contentDiv.Controls.Add(new ValidationSummary());

            Button submitButton = new Button();
            submitButton.ID = RegFormHelper.SUBMIT_BUTTON_ID;
            submitButton.Text = "Submit";           
            submitButton.Click += (s, args) => { };
            contentDiv.Controls.Add(submitButton);

            Button resetButton = new Button();
            resetButton.ID = RegFormHelper.RESET_BUTTON_ID;
            resetButton.Text = "Reset";
            resetButton.CausesValidation = false;
            resetButton.Click += ResetButton_Click;
            contentDiv.Controls.Add(resetButton);
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            ResetControls(content);
        }

        private void ResetControls(Control container)
        {
            foreach (Control item in container.Controls)
            {
                if (item.HasControls())
                {
                    ResetControls(item);
                }
                if(item is TextBox)
                {
                    (item as TextBox).Text = "";
                }
                if(item is Calendar)
                {
                    (item as Calendar).SelectedDate = DateTime.Now.Date;
                }
                if(item is DropDownList)
                {
                    (item as DropDownList).ClearSelection();
                }
            }
        }

        private HtmlTableRow MakeRow(string label)
        {
            HtmlTableRow row = new HtmlTableRow();
            HtmlTableCell labelCell = new HtmlTableCell();
            labelCell.InnerText = $"{label}:";
            row.Cells.Add(labelCell); 
            return row;            
        }

        private HtmlTableCell MakeInputCell(string inputID)
        {
            HtmlTableCell inputCell = new HtmlTableCell();
            TextBox inputTextBox = new TextBox();
            inputTextBox.ID = inputID;
            inputCell.Controls.Add(inputTextBox);
            return inputCell;
        }

        private HtmlTableCell MakeCalendarCell()
        {
            HtmlTableCell cell = new HtmlTableCell();

            Calendar calendar = new Calendar();
            calendar.SelectionMode = CalendarSelectionMode.Day;
            calendar.SelectionChanged += Calendar_SelectionChanged;

            cell.Controls.Add(calendar);

            TextBox text = new TextBox();
            text.ID = RegFormHelper.BIRTH_DATE_ID;
                        
            cell.Controls.Add(text);

            RangeValidator rVal = new RangeValidator();
            rVal.ControlToValidate = text.ID;
            rVal.Display = ValidatorDisplay.None;
            rVal.EnableClientScript = false;
            rVal.ErrorMessage = " Значение поля 'Дата рождения' должно находиться в диапазоне дат с 1го декабря 1960го года по сегодняшний день";
            rVal.Type = ValidationDataType.Date;
            rVal.MinimumValue = new DateTime(1960, 1, 1).ToShortDateString();
            rVal.MaximumValue = DateTime.Now.ToShortDateString();

            cell.Controls.Add(rVal);

            return cell;        
        }

        private void Calendar_SelectionChanged(object sender, EventArgs e)
        {
            (FindControl(RegFormHelper.BIRTH_DATE_ID) as TextBox).Text = (sender as Calendar).SelectedDate.ToShortDateString();
        }

        private HtmlTableCell MakeRequiredCitiesCell()
        {
            HtmlTableCell cell = new HtmlTableCell();

            DropDownList list = new DropDownList();
            list.ID = RegFormHelper.CITY_ID;
            list.Items.Add("Выберите город");

            cell.Controls.Add(list);

            CompareValidator reqVal = new CompareValidator();
            reqVal.Operator = ValidationCompareOperator.NotEqual;
            reqVal.ControlToValidate = list.ID;
            reqVal.ValueToCompare = "Выберите город";
            reqVal.EnableClientScript = false;
            reqVal.Display = ValidatorDisplay.None;
            reqVal.ErrorMessage = "Поле 'Город' должно быть заполнено";

            cell.Controls.Add(reqVal);

            return cell;
        }

        private HtmlTableCell MakeRequiredCountriesCell()
        {
            HtmlTableCell cell = new HtmlTableCell();

            DropDownList list = new DropDownList();
            list.Items.Add(new ListItem("Выберите страну"));
            list.Items.AddRange(countries.Select(c => new ListItem(c)).ToArray());
            list.ID = RegFormHelper.COUNTRY_ID;
            list.SelectedIndexChanged += List_SelectedIndexChanged;
            list.AutoPostBack = true;

            cell.Controls.Add(list);

            CompareValidator reqVal = new CompareValidator();
            reqVal.Operator = ValidationCompareOperator.NotEqual;
            reqVal.ControlToValidate = list.ID;
            reqVal.ValueToCompare = "Выберите страну";
            reqVal.EnableClientScript = false;
            reqVal.Display = ValidatorDisplay.None;
            reqVal.ErrorMessage = "Поле 'Страна' должно быть заполнено";

            cell.Controls.Add(reqVal);

            return cell;
        }

        private void List_SelectedIndexChanged(object sender, EventArgs e)
        {
            string country = (sender as DropDownList).SelectedValue;
            DropDownList cityList = FindControl(RegFormHelper.CITY_ID) as DropDownList;
            cityList.Items.Clear();
            cityList.Items.Add("Выберите город");
            cityList.Items.AddRange(cities.Where(kvp => kvp.Value == country)
                                          .Select(kvp => new ListItem(kvp.Key)).ToArray());
        }
        
        private RequiredFieldValidator MakeRequiredFieldValidator(string fieldName, string controlID)
        {
            RequiredFieldValidator reqValidator = new RequiredFieldValidator();
            reqValidator.ControlToValidate = controlID;
            reqValidator.Display = ValidatorDisplay.None;
            reqValidator.EnableClientScript = false;
            reqValidator.ErrorMessage = $"Поле '{fieldName}' должно быть заполнено";
            return reqValidator;
        }

        private CompareValidator MakeNotEqualValidator(string controlID, string controlToCompare, string message)
        {
            CompareValidator cVal = new CompareValidator();
            cVal.ControlToValidate = controlID;
            cVal.Display = ValidatorDisplay.None;
            cVal.EnableClientScript = false;
            cVal.Operator = ValidationCompareOperator.NotEqual;
            cVal.ControlToCompare = controlToCompare;
            cVal.ErrorMessage = message;
            return cVal;
        }
    }
}