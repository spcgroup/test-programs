﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RegForm
{
    public class RegFormHelper
    {
        public const string NICKNAME_ID = "nick";
        public const string FIRST_NAME_ID = "fName";
        public const string LAST_NAME_ID = "lName";
        public const string BIRTH_DATE_ID = "birthDate";
        public const string EMAIL_ID = "email";
        public const string COUNTRY_ID = "country";
        public const string CITY_ID = "city";
        public const string SUBMIT_BUTTON_ID = "submitButton";
        public const string RESET_BUTTON_ID = "resetButton";
    }
}