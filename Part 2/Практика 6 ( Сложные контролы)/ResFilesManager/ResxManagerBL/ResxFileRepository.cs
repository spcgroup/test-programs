﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Resources;
using ResFilesManager.Models;
using System.IO;
using System.Collections;
using System.ComponentModel.Design;
using System.Web.Configuration;

namespace ResFilesManager
{
    public static class ResxFileRepository
    {
        public static ICollection<ResxFileEntry> GetResources(string filename)
        {
            return GetResources(filename, 0, 0);
        }

        public static ICollection<ResxFileEntry> GetResources(string filename, int maximumRows, int startRowIndex)
        {   
            string path = Path.Combine(WebConfigurationManager.AppSettings[ResxHelper.CONFIG_FOLDER], filename ?? "");
            List<ResxFileEntry> resxEntriesList = new List<ResxFileEntry>();
            if (File.Exists(path) && Path.GetExtension(path).ToUpper() == ".RESX")
            {
                using (ResXResourceReader resxReader = new ResXResourceReader(path))
                {
                    resxReader.UseResXDataNodes = true;
                    try
                    {
                        IDictionaryEnumerator items = resxReader.GetEnumerator();
                        while (items.MoveNext())
                        {
                            ResXDataNode node = items.Value as ResXDataNode;
                            object value = node.GetValue((ITypeResolutionService)null);
                            if (value == null || value is string)
                            {
                                resxEntriesList.Add(new ResxFileEntry()
                                {
                                    Key = node.Name,
                                    Value = value?.ToString(),
                                    Comment = node.Comment ?? "",
                                    FileName = filename
                                });
                            }
                        }
                    }
                    catch { }
                }
            }
            if (startRowIndex >= 0 && maximumRows > 0)
            {
                return resxEntriesList.Skip(startRowIndex).Take(maximumRows).ToArray();
            }
            else
            {
                return resxEntriesList.ToArray();
            }
        }

        public static int GetCount(string filename)
        {
            int count = 0;
            string path = Path.Combine(WebConfigurationManager.AppSettings[ResxHelper.CONFIG_FOLDER], filename ?? "");
            List<ResxFileEntry> resxEntriesList = new List<ResxFileEntry>();
            if (File.Exists(path) && Path.GetExtension(path).ToUpper() == ".RESX")
            {
                using (ResXResourceReader resxReader = new ResXResourceReader(path))
                {
                    try
                    {
                        IDictionaryEnumerator items = resxReader.GetEnumerator();
                        while (items.MoveNext())
                        {
                            object value = items.Value;
                            if (value == null || value is string)
                            {
                                count++;
                            }
                        }
                    }
                    catch { }
                }
            }
            return count;
        }
        
        public static void AddResource(ResxFileEntry entry)
        {
            if(entry!=null)
            {
                List<ResxFileEntry> resList = GetResources(entry.FileName).ToList();
                if(resList.Where(e => e.Key == entry.Key).Count() == 0)
                {
                    resList.Add(entry);
                    CommitChanges(resList,entry.FileName);
                }
                else
                {
                    throw new InvalidOperationException($"Resource with key {entry.Key} is already exists");
                }
            }
        }

        public static void UpdateResource(ResxFileEntry entry)
        {
            UpdateResource(entry, null);
        }

        public static bool UpdateResource(ResxFileEntry entry,string oldKey=null)
        {
            if (entry != null)
            {                
                entry.Key = entry.Key.Trim();
                List<ResxFileEntry> resList = GetResources(entry.FileName).ToList();
                ResxFileEntry resourceToUpdate = resList.Where(e => e.Key == entry.Key).FirstOrDefault();
                if (resourceToUpdate!=null  )
                {
                    if (resourceToUpdate.Key == oldKey)
                    {
                        resourceToUpdate.Value = entry.Value?.Trim();
                        resourceToUpdate.Comment = entry.Comment?.Trim();
                    }
                    else
                    {
                        return false;
                    }
                }    
                else
                {
                    resList.RemoveAll(e => e.Key == oldKey);
                    resList.Add(entry);
                }            
                CommitChanges(resList, entry.FileName);
                return true;
            }
            return false;
        }

        public static void DeleteResource(ResxFileEntry entry)
        {
            if (entry != null)
            {
                List<ResxFileEntry> resList = GetResources(entry.FileName).ToList();
                if (resList.RemoveAll(e => e.Key == entry.Key) > 0)
                {
                    CommitChanges(resList, entry.FileName);
                }
            }
        }

        private static void CommitChanges(List<ResxFileEntry> entries,string filename)
        {
            string path= Path.Combine(WebConfigurationManager.AppSettings[ResxHelper.CONFIG_FOLDER], filename ?? "");
            if(Path.GetExtension(filename).ToUpper() == ".RESX")
            {
                if(File.Exists(path))
                {
                    try
                    {
                        File.Delete(path);
                        using (ResXResourceWriter resxWriter = new ResXResourceWriter(path))
                        {
                            foreach (ResxFileEntry item in entries)
                            {
                                resxWriter.AddResource(new ResXDataNode(item.Key, item.Value)
                                {
                                    Comment = item.Comment
                                });
                            }
                            resxWriter.Generate();
                        }
                    }
                    catch
                    {
                        throw new IOException($"Failed to update {path} file");
                    }
                }
            }
        }
    }
}