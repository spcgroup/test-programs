﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResFilesManager
{
    public class ResxHelper
    {
        public const string KEY_PROPRETY_NAME = "Key";
        public const string VALUE_PROPRETY_NAME = "Value";
        public const string COMMENT_PROPRETY_NAME = "Comment";
        public const string FILENAME_PROPRETY_NAME = "FileName";

        public const string CONFIG_FOLDER = "resFolder";
    }
}
