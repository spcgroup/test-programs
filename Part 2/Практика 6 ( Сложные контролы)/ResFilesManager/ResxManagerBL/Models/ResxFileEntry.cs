﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResFilesManager.Models
{
    public class ResxFileEntry
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public string Comment { get; set; }
        public string FileName { get; set; }
    }
}