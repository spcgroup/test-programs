﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.Configuration;
using ResFilesManager.Models;

namespace ResFilesManager
{
    public partial class _default : System.Web.UI.Page
    {
        public IEnumerable<string> GetFiles()
        {
            if (Directory.Exists(WebConfigurationManager.AppSettings[ResxHelper.CONFIG_FOLDER]))
            {
                return Directory.EnumerateFiles(WebConfigurationManager.AppSettings[ResxHelper.CONFIG_FOLDER],
                                                "*.resx", SearchOption.TopDirectoryOnly)
                                .Select(p => Path.GetFileName(p));
            }
            else
            {
                return Array.Empty<string>();
            }
        }

        protected void resGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert" && (e.CommandSource as Control).ID == "addResourceButton")
            {
                TextBox keyBox = resGrid.FooterRow.FindControl("addKeyBox") as TextBox;
                keyBox.Text = keyBox.Text.Trim();
                RequiredFieldValidator kVal = resGrid.FooterRow.FindControl("keyValidator") as RequiredFieldValidator;
                kVal.Enabled = true;
                Validate();
                ResxFileEntry entryToInsert = new ResxFileEntry()
                {
                    Key = keyBox.Text,
                    Value = (resGrid.FooterRow.FindControl("addValueBox") as TextBox).Text.Trim(),
                    Comment = (resGrid.FooterRow.FindControl("addCommentBox") as TextBox).Text.Trim(),
                    FileName = filesList.SelectedValue
                };
                if (!string.IsNullOrEmpty(entryToInsert.Key))
                {
                    try
                    {
                        ResxFileRepository.AddResource(entryToInsert);
                        resGrid.DataBind();
                    }
                    catch
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "addAlert",
                    "alert('Create failed! Make sure that Key field contains unique value');", true);
                    }
                    
                }
            }
        }

        protected void resGrid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
                ObjectDataSource source = (sender as GridView).DataSourceObject as ObjectDataSource;
                source.UpdateParameters.Add("oldKey", e.Keys[ResxHelper.KEY_PROPRETY_NAME].ToString());            
        }

        protected void resxDataSource_Updating(object sender, ObjectDataSourceMethodEventArgs e)
        {
            ObjectDataSource source = resGrid.DataSourceObject as ObjectDataSource;
            e.Cancel = true;
            ResxFileEntry entry = e.InputParameters[0] as ResxFileEntry;
            if (!ResxFileRepository.UpdateResource(e.InputParameters[0] as ResxFileEntry,
                source.UpdateParameters[0].DefaultValue))
            {
                ClientScript.RegisterStartupScript(GetType(), "updateAlert",
                    "alert('Update failed! Make sure that Key field contains unique value');", true);
            }
            
        }

        protected void valueBox_DataBinding(object sender, EventArgs e)
        {
            (sender as TextBox).Text = Server.HtmlDecode((sender as TextBox).Text);
        }

    }
}