﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace ResFilesManager
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            Server.ClearError();
            Response.Clear();
            if (ex is HttpRequestValidationException)
            {
                Response.Write("Operation failed! An incorrect data was entered.");
            }
            else
            {
                Response.Write($"Something bad!</br>Details: {ex.Message}");
            }
            Response.Write("</br><a href='/default.aspx'>Home</a>");
            Response.End();
        }
    }
}