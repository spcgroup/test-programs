﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="ResFilesManager._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="StyleSheet1.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        Please choose a file to work with: <asp:DropDownList ID="filesList" runat="server" SelectMethod="GetFiles"  AutoPostBack="true"/>
        <br />
        <asp:GridView ID="resGrid" runat="server" AllowPaging="true" PageSize="20" DataSourceID="resxDataSource"
             EnablePersistedSelection="true" AutoGenerateColumns="false" DataKeyNames="Key,FileName"
             ShowFooter="true" OnRowCommand="resGrid_RowCommand"  OnRowUpdating="resGrid_RowUpdating" >
            <Columns>
                <asp:TemplateField HeaderText="Key">
                    <ItemTemplate>
                        <span runat="server"><%#: Eval(ResFilesManager.ResxHelper.KEY_PROPRETY_NAME) %></span>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="keyBox" runat="server" Text='<%#: Bind("Key") %>' AutoPostBack="false"/> 
                        <asp:RequiredFieldValidator EnableClientScript="false" ControlToValidate="keyBox" Display="Dynamic"
                                runat="server"  ForeColor="Red" ErrorMessage="Resource key should not be empty"/>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="addKeyBox" Text="" runat="server" AutoPostBack="false"/>
                        <div>
                            <asp:RequiredFieldValidator EnableClientScript="false" ControlToValidate="addKeyBox" Display="Dynamic"
                                runat="server"  ForeColor="Red" ErrorMessage="Resource key should not be empty" ID="keyValidator"
                                Enabled="false"/>
                        </div>
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Value">
                    <ItemTemplate>
                        <span runat="server"><%#: Eval(ResFilesManager.ResxHelper.VALUE_PROPRETY_NAME) %></span>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="valueBox" runat="server" Text='<%#: Bind("Value") %>' ValidateRequestMode="Disabled" OnDataBinding="valueBox_DataBinding"/>                        
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="addValueBox" Text="" runat="server"  AutoPostBack="false" ValidateRequestMode="Disabled" OnDataBinding="valueBox_DataBinding"/>                        
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Comment">
                    <ItemTemplate>
                        <span runat="server"><%#: Eval(ResFilesManager.ResxHelper.COMMENT_PROPRETY_NAME) %></span>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:TextBox ID="commentBox" runat="server" Text='<%#: Bind("Comment") %>' AutoPostBack="false"/>                        
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:TextBox ID="addCommentBox" Text="" runat="server"  AutoPostBack="false" />                        
                    </FooterTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="FileName" Visible="false" />

                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton Text="Edit" CommandName="Edit" runat="server" CausesValidation="false"/>
                        <asp:LinkButton Text="Delete" CommandName="Delete" runat="server"  CausesValidation="false"/>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:LinkButton Text="Update" CommandName="Update" runat="server" />
                        <asp:LinkButton Text="Cancel" CommandName="Cancel" runat="server" CausesValidation="false"/>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:Button id="addResourceButton" Text="Add New Resource" CommandName="Insert" runat="server"/>
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>

            <EmptyDataTemplate>
                File not found or string resources haven't been found in current file
            </EmptyDataTemplate>

        </asp:GridView>

        <asp:ObjectDataSource id="resxDataSource" TypeName="ResFilesManager.ResxFileRepository" runat="server"
            DataObjectTypeName="ResFilesManager.Models.ResxFileEntry" EnablePaging="true"
            SelectMethod="GetResources"
            UpdateMethod="UpdateResource"
            DeleteMethod="DeleteResource"
            InsertMethod="AddResource"
            SelectCountMethod="GetCount"
            OnUpdating="resxDataSource_Updating">           
            <SelectParameters>
                <asp:ControlParameter ControlID="filesList" PropertyName="SelectedValue" Name="filename" Type="String"/>               
            </SelectParameters>                      
        </asp:ObjectDataSource>
    </div>
    </form>
</body>
</html>
