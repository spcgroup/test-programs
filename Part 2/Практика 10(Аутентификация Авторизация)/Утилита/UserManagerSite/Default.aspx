﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="UserManagerSite.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="css/styles.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:ListView SelectMethod="GetUsers" DeleteMethod="DeleteUser" ItemPlaceholderID="items" id="usersListView"
            ItemType="UserManagerSite.Models.UserDetails" runat="server"  InsertItemPosition="LastItem" InsertMethod="AddUser"
            DataKeyNames="UserName" EnablePersistedSelection="true">
            <LayoutTemplate>
                <table class="list-table">
                    <tr><th></th><th>User Name</th><th>E-mail</th></tr>
                    <tr id="items" runat="server"></tr>
                    <tr>
                        <td colspan="4">
                            <asp:DataPager runat="server" PageSize="10" >
                                <Fields>
                                    <asp:NextPreviousPagerField ShowNextPageButton="true" ShowPreviousPageButton="true"
                                        NextPageText="Next" PreviousPageText="Previous" ButtonType="Button"/>
                                </Fields>
                            </asp:DataPager>
                        </td>
                    </tr>
                </table>
            </LayoutTemplate>
            <SelectedItemTemplate>
                <tr class="selected-row">
                    <td><asp:LinkButton CommandName="Select" Text="Select" runat="server" /></td>
                    <td><%#: Item.UserName %></td>
                    <td><%#: Item.Email %></td>
                    <td><asp:LinkButton CommandName="Delete" Text="Delete" runat="server" /></td>
                </tr>
            </SelectedItemTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:LinkButton CommandName="Select" Text="Select" runat="server" /></td>
                    <td><%#: Item.UserName %></td>
                    <td><%#: Item.Email %></td>
                    <td><asp:LinkButton CommandName="Delete" Text="Delete" runat="server" /></td>
                </tr>
            </ItemTemplate>
            <InsertItemTemplate>
                <tr>
                    <td>New user:</td>
                    <td><input type="text" id="userName" runat="server" value="<%#: BindItem.UserName %>" /></td>
                    <td><input type="text" id="email" runat="server" value="<%#: BindItem.Email %>" /></td>
                    <td><asp:Button CommandName="Insert" Text="Add New User" runat="server" /></td>
                </tr>
            </InsertItemTemplate>
            <EmptyDataTemplate>
                No users exists in the database
            </EmptyDataTemplate>
        </asp:ListView>
        <asp:Label CssClass="output" id="usersListLog" Visible="false" runat="server"></asp:Label>
        <asp:PlaceHolder ID="detailsPh" runat="server" Visible="false">
            <asp:FormView ID="userDetailsView" DefaultMode ="Edit" SelectMethod="GetUser" UpdateMethod="UpdateUser" 
                ItemType="UserManagerSite.Models.UserDetails" runat="server" DataKeyNames="UserName" RenderOuterTable="false">
                <EditItemTemplate>
                    <div>
                        <asp:ValidationSummary runat="server" CssClass="error"/>
                        <table class="details-table">
                            <tr><th>Field</th><th>Value</th></tr>
                            <tr><td>User Name</td><td><%#: Item.UserName %></td></tr>
                            <tr><td>Email</td><td><input type="text" id="email" value="<%#: BindItem.Email %>" runat="server"/></td></tr>
                            <tr><td>Password</td><td><input type="text" id="password" value="<%#: BindItem.Password %>" runat="server" /></td></tr>
                            <tr><td>Confirm Password</td><td><input type="text" id="confirm" value="<%#: BindItem.Confirm %>" runat="server" /></td></tr>
                            <tr><td colspan="2"><asp:Button CommandName="Update" Text="Update" runat="server" /></td></tr>
                        </table>
                    </div>
                </EditItemTemplate>
            </asp:FormView>
            <asp:Label CssClass="output" id="userDetailsLog" runat="server" Visible="false"></asp:Label>
        </asp:PlaceHolder>
    </div>
    </form>
</body>
</html>
