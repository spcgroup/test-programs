﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace UserManagerSite.Models
{
    public class UserDetailsRepository
    {
        public List<UserDetails> UserDetailsList { get; set; }

        public UserDetailsRepository()
        {
            UserDetailsList = new List<UserDetails>();
            MembershipUserCollection users = Membership.GetAllUsers();
            foreach (MembershipUser item in users)
            {
                UserDetailsList.Add(new UserDetails()
                {
                    UserName = item.UserName,
                    Email = item.Email
                });
            }
        }

        public string AddUser(UserDetails userDetails)
        {
            try
            {
                if (userDetails == null)
                {
                    return "Данные для добавления не были переданы";
                }
                string password = Membership.GeneratePassword(10, 1);
                Membership.CreateUser(userDetails.UserName, password, userDetails.Email);
                return "Пользователь успешно добавлен. Пароль: " + password;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public string UpdateUser(UserDetails userDetails)
        {
            try
            {
                if (userDetails == null || userDetails.UserName == null)
                {
                    return "Данные для изменения не были переданы";
                }
                MembershipUser user = Membership.GetUser(userDetails.UserName);
                if(user == null)
                {
                    return $"Пользователь '{userDetails.UserName}' не найден";
                }
                string password = user.ResetPassword();
                bool pswdChanged = user.ChangePassword(password, userDetails.Password);
                if (pswdChanged)
                {
                    user = Membership.GetUser(userDetails.UserName);
                    user.Email = userDetails.Email;
                    Membership.UpdateUser(user);
                    return "Данные пользователя успешно изменены";
                }
                else
                {
                    return "Ошибка при попытке изменения пароля. Пароль был сброшен. Новый пароль: "+password;
                }

            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public string DeleteUser(string userName)
        {
            if( Membership.DeleteUser(userName))
            {
                return $"Пользователь '{userName}' успешно удален";
            }
            else
            {
                return $"Пользователь '{userName}' не был удален или не найден";
            }
        }
    }
}