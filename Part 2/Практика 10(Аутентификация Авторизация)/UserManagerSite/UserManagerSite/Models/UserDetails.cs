﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.Design;
using System.ComponentModel.DataAnnotations;

namespace UserManagerSite.Models
{
    public class UserDetails
    {   
        [Key]
        public string UserName { get; set; }
        
        public string Email { get; set; }

        [Required]
        [MinLength(4,ErrorMessage = "Пароль должен содержать не менее 4 символов")]
        public string Password { get; set; }

        [Required]
        [Compare("Password",ErrorMessage = "Пароли должны совпадать")]
        public string Confirm { get; set; }
    }
}