﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.ModelBinding;
using UserManagerSite.Models;

namespace UserManagerSite
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            userDetailsLog.Visible = false;
            usersListLog.Visible = false;
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                detailsPh.Visible = usersListView.SelectedIndex != -1;
                userDetailsView.DataBind();
            }
        }
        
        public IQueryable<UserDetails> GetUsers()
        {
            return new UserDetailsRepository().UserDetailsList.AsQueryable();
        }

        public UserDetails GetUser()
        {
            string userName = usersListView.SelectedValue?.ToString();
            if(userName != null)
            {
                return new UserDetailsRepository().UserDetailsList.FirstOrDefault(u => u.UserName == userName);
            }
            else
            {
                return new UserDetails();
            }
        }

        public void AddUser(UserDetails userDetails)
        {
            usersListLog.Visible = true;
            usersListLog.Text = new UserDetailsRepository().AddUser(userDetails);
            detailsPh.Visible = false;
            usersListView.DataBind();
            usersListView.SelectedIndex = -1;
        }

        public void DeleteUser(string userName)
        {
            usersListLog.Visible = true;
            usersListLog.Text = new UserDetailsRepository().DeleteUser(userName);
        }

        public void UpdateUser(UserDetails userDetails)
        {
            if (ModelState.IsValid)
            {
                userDetailsLog.Visible = true;
                userDetailsLog.Text = new UserDetailsRepository().UpdateUser(userDetails);
                usersListView.DataBind();
            }
            
        }

    }
}