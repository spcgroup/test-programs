﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Controls
{
    public class AnswerSelectedEventArgs : EventArgs
    {
        public AnswerSelectedEventArgs(Question question,Answer selectedAnswer)
        {
            Question = question;
            SelectedAnswer = selectedAnswer;
        }
        public Question Question { get; set; }
        public Answer SelectedAnswer { get; set; }
    }
}
