﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Questions.Controls
{
    [ParseChildren(true, "Answers")]
    public class Question
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public List<Answer> Answers { get; set; } = new List<Answer>();

        private int _correctIndex;
        public int CorrectIndex
        {
            get
            {
                return _correctIndex;
            }
            set
            {
                _correctIndex = value >= 0 ?  value : 0;
            }
        }

        [Browsable(false)]
        public Answer CorrectAnswer {
            get
            {
                return Answers.Count > CorrectIndex ? Answers[CorrectIndex] : null;
            }
        }

        [Browsable(false)]
        public int SelectedIndex { get; set; } = -1;

        [Browsable(false)]
        public bool IsAnswered
        {
            get
            {
                return SelectedIndex == -1;
            }
        }

        [Browsable(false)]
        public Answer SelectedAnswer
        {
            get
            {
                return SelectedIndex >= 0 && Answers.Count > SelectedIndex ? Answers[SelectedIndex] : null;
            }
        }

        public string Text { get; set; }
    }
}
