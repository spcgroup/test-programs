﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;

namespace Questions.Controls
{
    [ParseChildren(true,"Questions")]
    public class QuestionsList: CompositeControl
    {
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        [PersistenceMode(PersistenceMode.InnerDefaultProperty)]
        public List<Question> Questions { get; set; }

        public int QuestionFontSize { get; set; } = 20;

        public int AnswerFontSize { get; set; } = 14;

        public string SelectedCssClass { get; set; }

        public event EventHandler<AnswerSelectedEventArgs> AnswerSelected;

        public event EventHandler<CompleteTestButtonClickEventArgs> CompletTestButtonClick;

        HtmlGenericControl info = new HtmlGenericControl("p");

        public QuestionsList()
        {
            Load += Page_Load;
        }

        private void Page_Load(object sender, EventArgs e)
        {
            if (Questions != null)
            {
                for (int i = 0; i < Questions.Count; i++)
                {
                    RadioButtonList rbl = FindControl(GetID($"rbl{i}")) as RadioButtonList;
                    if (rbl != null && rbl.SelectedIndex >= 0 && rbl.SelectedIndex < Questions[i].Answers.Count)
                    {
                        Questions[i].SelectedIndex = rbl.SelectedIndex;
                        if (SelectedCssClass != null)
                        {
                            rbl.SelectedItem.Attributes.Add("class", SelectedCssClass);
                        }
                    }
                    else
                    {
                        Questions[i].SelectedIndex = -1;
                    }
                }
                info.InnerText = $"Дано ответов {Questions.Count(q => q.SelectedIndex != -1)}/{Questions.Count}";
            }
        }

        protected override void CreateChildControls()
        {
            if (Questions != null)
            {
                HtmlGenericControl questionsList = new HtmlGenericControl("ol");
                FillQuestionsList(questionsList);
                HtmlGenericControl contentDiv = new HtmlGenericControl("div");
                contentDiv.Controls.Add(questionsList);
                contentDiv.Controls.Add(info);
                Controls.Add(contentDiv);

                Button finishButton = new Button();
                finishButton.Text ="Завершить тест";
                finishButton.Click += (s, e) =>
                {
                    if (CompletTestButtonClick != null)
                    {
                        CompletTestButtonClick(finishButton, new CompleteTestButtonClickEventArgs(Questions));
                    }
                };

                HtmlGenericControl buttonDiv = new HtmlGenericControl("div");
                buttonDiv.Controls.Add(finishButton);
                Controls.Add(buttonDiv);
            }
            else
            {
                Label notFoundText = new Label();
                notFoundText.Text = "No questions added";
                Controls.Add(notFoundText);
            }
            base.CreateChildControls();
        }

        private void FillQuestionsList(HtmlGenericControl questionsList)
        {
            int index = 0;
            foreach (Question question in Questions)
            {
                HtmlGenericControl qListItem = new HtmlGenericControl("li");
                qListItem.Attributes.Add("style", $"font-size:{QuestionFontSize}px;");
                qListItem.InnerText = question.Text;

                RadioButtonList curAnswers = new RadioButtonList();
                curAnswers.AutoPostBack = true;
                curAnswers.ID = GetID($"rbl{index++}");
                curAnswers.SelectedIndexChanged += (sender, e) =>
                {
                    if (AnswerSelected != null)
                    {
                        AnswerSelected(this, new AnswerSelectedEventArgs(question, question.SelectedAnswer));
                    }
                };

                foreach (Answer answer in question.Answers)
                {
                    ListItem item = new ListItem(answer.Text);
                    item.Attributes.Add("style", $"font-size:{AnswerFontSize}px;");
                    curAnswers.Items.Add(item);
                }
                qListItem.Controls.Add(curAnswers);
                questionsList.Controls.Add(qListItem);
            }
        }

        private string GetID(string name)
        {
            return $"{ClientID}{ClientIDSeparator}{name}";
        }
    } 
}
