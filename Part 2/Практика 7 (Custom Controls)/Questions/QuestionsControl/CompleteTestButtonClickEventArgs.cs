﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Questions.Controls
{
    public class CompleteTestButtonClickEventArgs:EventArgs
    {
        public CompleteTestButtonClickEventArgs(List<Question> questions)
        {
            Questions = questions.ToArray();
        }
        public Question[] Questions { get; set; }
    }
}
