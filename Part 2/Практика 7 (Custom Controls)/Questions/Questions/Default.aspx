﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Questions.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        .selected_Answ{
            background-color:aqua;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <CC:QuestionsList QuestionFontSize="20" AnswerFontSize="14"
            OnCompletTestButtonClick="Unnamed_CompletTestButtonClick" SelectedCssClass="selected_Answ"
            OnAnswerSelected="Unnamed_AnswerSelected" runat="server">
            <CC:Question CorrectIndex="0" Text="Вопрос 1">
                <CC:Answer Text="Ответ 1" />
                <CC:Answer Text="Ответ 2" />
                <CC:Answer Text="Ответ 3" />
            </CC:Question>
            <CC:Question CorrectIndex="2" Text="Вопрос 2">
                <CC:Answer Text="Ответ 1" />
                <CC:Answer Text="Ответ 2" />
                <CC:Answer Text="Ответ 3" />
            </CC:Question>
            <CC:Question CorrectIndex="1" Text="Вопрос 3">
                <CC:Answer Text="Ответ 1" />
                <CC:Answer Text="Ответ 2" />
                <CC:Answer Text="Ответ 3" />
            </CC:Question>
        </CC:QuestionsList>  
    </div>
    </form>
</body>
</html>
