﻿(function () {
    var validateInput = function () {
        var input = this;
        var text = input.value;
        var validator = validationFuncs[input.id];
        var errorMessage = validator ? validator(text) : null;
        if (errorMessage){
            if(!(input.nextElementSibling && input.nextElementSibling.classList.contains("error"))) {
                var message = document.createElement("p");
                message.className = "error";
                message.innerHTML = errorMessage;
                input.parentElement.insertBefore(message, input.nextElementSibling);
            }
        }
        else{
            if (input.nextElementSibling && input.nextElementSibling.className == "error") {
                input.parentElement.removeChild(input.nextElementSibling);
            }
        }
    };

    var validationFuncs = {};

    validationFuncs.login = function (text) {
        var pattern = new RegExp("^[A-Za-z0-9]*[A-Z][A-Za-z0-9]*[0-9]$");
        if (text.length < 5 || !pattern.test(text)) {
            return "Логин должен содержать не менее 5 латинских букв или цифр," +
                 " минимум 1 букву в верхнем регистре и заканчиваться цифрой!";
        }
        else {
            return null;
        }
    };

    validationFuncs.pswd = function (text) {
        var patt1 = new RegExp("[A-Z]");
        var patt2 = new RegExp("[a-z]");
        var patt3 = new RegExp("[0-9]");
        var patt4 = new RegExp("\\W");
        var count = 0;
        if (patt1.test(text)) count++;
        if (patt2.test(text)) count++;
        if (patt3.test(text)) count++;
        if (patt4.test(text)) count++;
        if (count >= 3) {
            return null;
        }
        else {
            return "Пароль должен содержать символы любых 3х категроий из списка:<ul>" +
                "<li>прописные буквы(A-Z)</li>" +
                "<li>строчные буквы(a-z)</li>" +
                "<li>десятичные цифры</li>" +
                "<li>неалфавитные симвлоы(например !, %, $, #)</li></ul>";
        }
    };

    validationFuncs.pswdConfirm = function (text) {
        var target = document.getElementById("pswd").value;
        return text === target ? null : "Пароли должны совпадать";
    };

    validationFuncs.name = function (text) {
        var pattern = new RegExp("^[A-Za-zА-Яа-я]+ [A-Za-zА-Яа-я]+ [A-Za-zА-Яа-я]+ *$");
        return pattern.test(text) ? null : "ФИО может содержать только символы и должно состоять из 3х слов, разделенных пробелом";
    };

    validationFuncs.birthDate = function (text) {
        var parts = text.split("/");
        parts.reverse();
        var dateString= parts.join("/");
        var timestamp = Date.parse(dateString);
        var ageLabel = document.getElementById("age");
        ageLabel.innerText = "Возраст: ";
        if (isNaN(timestamp)) {
            return "Дата рождения должна быть валидной датой в формате ДД/ММ/ГГГГ";
        }
        else
        {
            var today = new Date();
            var ageMs = today.getTime()-timestamp;
            var age = new Date(ageMs);
            var years = age.getFullYear() - 1970;
            if(years < 12)
            {
                return "Регистрация только с 12";
            }
            else
            {
                ageLabel.innerText = ageLabel.innerText + years;
                return null;
            }
        }
    };

    validationFuncs.index = function (text) {
        var pattern = new RegExp("^[0-9]{5}$");
        return pattern.test(text) ? null : "Почтовый индекс должен содержать только 5 цифр";
    };

    validationFuncs.email = function (text) {
        var pattern = new RegExp("^[A-Za-z0-9_]+\@[A-Za-z0-9_.]+\\.[A-Za-z]+$");
        return pattern.test(text) ? null : "Поле email должно содержать действительный адрес электронной почты";
    };

    var toggleSubscrList = function () {
        if (this.checked) {
            document.getElementById("subscrList").style.display = "block";
        }
        else {
            document.getElementById("subscrList").style.display = "";
        }
    };

    var subscrListChanged = function()
    {
        if (!document.getElementById("subscription").checked) { return; }
        if (this.nextElementSibling && this.nextElementSibling.classList.contains("error")) {
            this.parentElement.removeChild(this.nextElementSibling);
        }
        var inputs = this.querySelectorAll("input");
        for (var i = 0; i < inputs.length; i++) {
            if (inputs[i].checked) { return;}
        }
        var message = document.createElement("p");
        message.className = "error";
        message.innerHTML = "Необходимо выбрать хотя бы одну подписку";
        this.parentElement.appendChild(message);
    }

    var nextPage = function (event) {
        var curPage = document.querySelector(".active-page");
        var fields = curPage.querySelectorAll(".form-field");
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].onchange) {
                fields[i].onchange();
            }
        }
        var errors = curPage.querySelectorAll(".error");
        if (errors.length === 0) {
            var nextPage = document.querySelector(".active-page + .page");
            if (nextPage) {
                document.getElementById("title").innerHTML = nextPage.title;
                curPage.classList.remove("active-page");
                nextPage.classList.add("active-page");
                if (nextPage.classList.contains("last-page")) {
                    this.setAttribute("disabled","disabled");
                    document.getElementById("complete").style.display = "";
                    fillTable();
                }
                document.getElementById("prev").removeAttribute("disabled");
            }
        };
        event.preventDefault();
    };

    var fillTable = function () {
        var table = document.getElementById("summary");
        var tableBody = document.createElement("tbody");
        var labels = document.getElementsByClassName("form-label");
        var inputs = document.querySelectorAll(".form-label + .form-field");
        for (var i = 0; i < inputs.length; i++) {
            var row = document.createElement("tr");
            var name =  labels[i].innerHTML;
            var value = inputs[i].value;
            row.innerHTML = "<td>"+name+"</td><td>"+value+"</td>";
            tableBody.appendChild(row);
        }
        if (document.getElementById("subscription").checked){
            var row = document.createElement("tr");
            var subscriptions= document.getElementById("subscrList");
            var name = subscriptions.querySelector("legend").innerHTML;
            var checkboxInputs = subscriptions.querySelectorAll("input");
            var values = "";
            for (var i = 0; i < checkboxInputs.length; i++) {
                if (checkboxInputs[i].checked) {
                    values = values + "<li>" + checkboxInputs[i].value + "</li>";
                };
            }
            row.innerHTML = "<td>" + name + "</td><td>" + values + "</td>";
            tableBody.appendChild(row);
        }
        if (table.querySelector("tbody")){
            table.removeChild(table.querySelector("tbody"));
        }
        table.appendChild(tableBody);
    }

    var prevPage = function (event) {
        var curPage = document.querySelector(".active-page");
        var prevPage = curPage.previousElementSibling;
        document.getElementById("title").innerHTML = prevPage.title;
        curPage.classList.remove("active-page");
        prevPage.classList.add("active-page");
        document.getElementById("next").removeAttribute("disabled");
        if(prevPage.classList.contains("first-page")){
            this.setAttribute("disabled","disabled");
        }
        event.preventDefault();
    };

    var sendForm = function () {
        var fields = document.querySelectorAll(".form-field");
        for (var i = 0; i < fields.length; i++) {
            if (fields[i].onchange) {
                fields[i].onchange();
            }
        }
        var errors = document.getElementsByClassName("error");
        if (!errors.length) {
            alert("Регистрация прошла успешно");
        }
    };

    var ready = function () {
        var textfields = document.querySelectorAll("input[type=text], input[type=password]");
        for (var i = 0; i < textfields.length; i++) {
            textfields[i].onchange = validateInput;
        }
        document.getElementById("subscription").onchange = toggleSubscrList;
        document.getElementById("subscrList").onchange = subscrListChanged;

        document.getElementById("complete").style.display = "none";
        document.getElementById("complete").onclick = sendForm;

        document.getElementById("next").onclick = nextPage;
        document.getElementById("prev").onclick = prevPage;

        document.getElementById("title").innerHTML = document.getElementsByClassName("active-page")[0].title;
    };
    document.addEventListener("DOMContentLoaded", ready);
}
)();