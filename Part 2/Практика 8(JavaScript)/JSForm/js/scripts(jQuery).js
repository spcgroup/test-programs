﻿(function () {
    var validateInput = function () {
        var $input = $(this);
        var validator = validationFuncs[$input.attr("id")];
        var errorMessage = validator ? validator($input.val()) : null;
        if (errorMessage ) {
            if(!$input.next().hasClass("error")){
                var $message = $("<p>").addClass("error").html(errorMessage);
                $input.after($message);
            } 
        }
        else
        {
            $input.next().remove(".error");
        }
    };

    var validationFuncs = {};

    validationFuncs.login = function (text) {
        var pattern = new RegExp("^[A-Za-z0-9]*[A-Z][A-Za-z0-9]*[0-9]$");
        if (text.length < 5 || !pattern.test(text)) {
            return "Логин должен содержать не менее 5 латинских букв или цифр," +
                 " минимум 1 букву в верхнем регистре и заканчиваться цифрой!";
        }
        else {
            return null;
        }
    };

    validationFuncs.pswd = function (text) {
        var patt1 = new RegExp("[A-Z]");
        var patt2 = new RegExp("[a-z]");
        var patt3 = new RegExp("[0-9]");
        var patt4 = new RegExp("\\W");
        var count = 0;
        if (patt1.test(text)) count++;
        if (patt2.test(text)) count++;
        if (patt3.test(text)) count++;
        if (patt4.test(text)) count++;
        if (count >= 3) {
            return null;
        }
        else {
            return "Пароль должен содержать символы любых 3х категроий из списка:<ul>" +
                "<li>прописные буквы(A-Z)</li>" +
                "<li>строчные буквы(a-z)</li>" +
                "<li>десятичные цифры</li>" +
                "<li>неалфавитные симвлоы(например !, %, $, #)</li></ul>";
        }
    };

    validationFuncs.pswdConfirm = function (text) {
        var target = $("#pswd").val();
        return text === target ? null : "Пароли должны совпадать";
    };

    validationFuncs.name = function (text) {
        var pattern = new RegExp("^[A-Za-zА-Яа-я]+ [A-Za-zА-Яа-я]+ [A-Za-zА-Яа-я]+ *$");
        return pattern.test(text) ? null : "ФИО может содержать только символы и должно состоять из 3х слов, разделенных пробелом";
    };

    validationFuncs.birthDate = function (text) {
        var parts = text.split("/");
        parts.reverse();
        var dateString = parts.join("/");
        var timestamp = Date.parse(dateString);
        var ageLabel = document.getElementById("age");
        ageLabel.innerText = "Возраст: ";
        if (isNaN(timestamp)) {
            return "Дата рождения должна быть валидной датой в формате ДД/ММ/ГГГГ";
        }
        else {
            var today = new Date();
            var ageMs = today.getTime() - timestamp;
            var age = new Date(ageMs);
            var years = age.getFullYear() - 1970;
            if (years < 12) {
                return "Регистрация только с 12";
            }
            else {
                ageLabel.innerText = ageLabel.innerText + years;
                return null;
            }
        }
    };

    validationFuncs.index = function (text) {
        var pattern = new RegExp("^[0-9]{5}$");
        return pattern.test(text) ? null : "Почтовый индекс должен содержать только 5 цифр";
    };

    validationFuncs.email = function (text) {
        var pattern = new RegExp("^[A-Za-z0-9_]+\@[A-Za-z0-9_.]+\\.[A-Za-z]+$");
        return pattern.test(text) ? null : "Поле email должно содержать действительный адрес электронной почты";
    };

    var toggleSubscrList = function () {
        if ($(this).prop("checked")) {
            $("#subscrList").css("display", "block");
        }
        else {
            $("#subscrList").css("display", "");
        }
    };

    var subscrListChanged = function () {
        if (!$("#subscription").prop("checked")) { return; }
        var $fieldset = $(this);
        $fieldset.next().remove(".error");
        if ($fieldset.find(":checked").length) { return; }
        var $message = $("<p>").addClass("error").text("Необходимо выбрать хотя бы одну подписку");
        $fieldset.after($message);
    }

    var nextPage = function () {
        var $curPage = $(".active-page");
        $curPage.find(".form-field").change();
        if (!$curPage.find(".error").length) {
            var $nextPage = $curPage.next(".page");
            if ($nextPage.length) {
                document.getElementById("title").innerHTML = $nextPage.attr("title");
                $curPage.removeClass("active-page");
                $nextPage.addClass("active-page");
                if ($nextPage.hasClass("last-page")) {
                    $(this).prop("disabled",true);
                    $("#complete").css("display","");
                    fillTable();
                }
                $("#prev").prop("disabled",false);
            }
        };
        event.preventDefault();
    };

    var fillTable = function () {
        var $table = $("#summary");
        var $rows = $(".form-label").map(function () {
            var $name = $("<td>").text($(this).text());
            var $value = $("<td>").text($(this).next().val());
            return $("<tr>").append($name).append($value);
        })    
        if ($("#subscription").prop("checked")) {
            var $subscrName = $("<td>").text($("#subscrList legend").text());
            var $subscrValues = $("<td>");
            $("#subscrList input:checked").map(function () {
                return $("<li>").text($(this).val());
            }).each(function () {
                $subscrValues.append($(this));
            });
            var $subsRow = $("<tr>").append($subscrName).append($subscrValues);
        };
        var $newBody = $("<tbody>");
        $rows.each(function () {
            $newBody.append($(this));
        });
        $newBody.append($subsRow);
        var $tbody = $table.find("tbody");
        $tbody.length ? $tbody.replaceWith($newBody) : $table.append($newBody);
    }

    var prevPage = function () {
        var $curPage = $(".active-page");
        var $prevPage = $curPage.prev();
        $("#title").text($prevPage.attr("title"));
        $curPage.removeClass("active-page");
        $prevPage.addClass("active-page");
        $("#next").prop("disabled",false);
        if ($prevPage.hasClass("first-page")) {
            $(this).prop("disabled",true);
        }
        event.preventDefault();
    };

    var sendForm = function () {
        $(".form-field").change();
        if (!$("#content").find(".error").length) {
            alert("Регистрация прошла успешно");
        }
    };

    $(document).ready(function () {
        $(":text, :password").on("change", validateInput);

        $("#subscription").on("change", toggleSubscrList);
        $("#subscrList").on("change",subscrListChanged);

        $("#complete").css("display", "none");
        $("#complete").on("click", sendForm);

        $("#next").on("click",nextPage);
        $("#prev").on("click",prevPage);

        $("#title").text($(".active-page").attr("title"));
    });
}
)(jQuery);